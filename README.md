# Space Maths



### Presentation du jeu
Space Maths est un super jeu éducatif basé sur les mathématiques et destiné aux enfants de 6 à 12 ans. 
Apprend les mathématiques et renforce tes connaissances par la pratique du calcul mental tout en t'amusant !
Choisis ton avatar, personnalise le et aide le à mener à bien sa mission spatiale en explorant des planètes lointaines.
Le jeu est disponible gratuitement sur tous les navigateurs principaux et ne nécessite pas de connexion internet une fois installé pour y jouer ou sauvegarder sa progression.

### Liens

- Pour jouer : https://louiskls.gitlab.io/pi-jeu-educatif/index.html
- [Documentation](https://louiskls.gitlab.io/pi-jeu-educatif/documentation.html)
- [Code source](https://gitlab.com/LouisKls/pi-jeu-educatif/)

### Licence

Le code est sous licence libre  [GNU GPL v3](https://www.gnu.org/licenses/)
- [Crédits](https://louiskls.gitlab.io/pi-jeu-educatif/credits.html)
