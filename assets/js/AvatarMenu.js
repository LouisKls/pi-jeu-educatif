/*
  Classe controlant le menu de personnalisation de l'avatar.

  Elle contient :
  - Le joueur et ses informations en paramètre

  Elle gère :
  - L'affichage de l'avatar du joueur selon les objets qu'il a équipé par superposition
  - L'affichage des objets possédés par le joueur sous forme d'iconnes
  - L'affichage des boutons interactifs
  - L'ajout d'une iconne sur l'objet choisi pour notifier la selection
  - Le changement des objets équipés par le joueur (données)
  - La sauvegarde de l'avatar lorsque le joueur quitte le menu

*/

function AvatarMenu(player) {

  this.player = player; // Objet joueur


  // Coordonnées iconne de fermeture
  const crossIconCoords = {
    x: canvas.width - 107,
    y: 82,
    radius: 50
  }


  // Coordonnées image avatar
  const avatarImageCoords = {
    x: (canvas.width/2) - 130,
    y: 150,
    width: 250,
    height: 470
  }



 // Handler de click pour listener
  var avatarMenuClickHandler = function(e) {

    var it = controller.avatarMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };


    // Vérifie click sur l'iconne de fermeture
    if(isCircleIntersect(mouse, crossIconCoords)) {
      controller.saveProfile();
      it.closeAvatarMenuScene();
    }


    // Verifie interactions avec objets
    it.player.ownedObjects.forEach(object => {
      if (isAvatarObjectIntersect(mouse, object)) {
        // Si l'objet n'est pas déjà équipé -> équiper
        if(!it.isEquiped(object.name)) {
          if(object.type == 'head') {
            it.player.equipedObjects.head = object.name;
          } else if(object.type == 'body') {
            it.player.equipedObjects.body = object.name;
          } else if(object.type == 'hands') {
            it.player.equipedObjects.hands = object.name;
          } else if(object.type == 'legs') {
            it.player.equipedObjects.legs = object.name;
          } else if(object.type == 'feet') {
            it.player.equipedObjects.feet = object.name;
          }
        // Sinon l'objet est déjà équipé -> déséquiper
        } else {
          if(object.type == 'head') {
            it.player.equipedObjects.head = null;
          } else if(object.type == 'body') {
            it.player.equipedObjects.body = null;
          } else if(object.type == 'hands') {
            it.player.equipedObjects.hands = null;
          } else if(object.type == 'legs') {
            it.player.equipedObjects.legs = null;
          } else if(object.type == 'feet') {
            it.player.equipedObjects.feet = null;
          }
        }

        it.removeClickListener();
        controller.displayAvatarMenu(); // Menu rechargé
      }
    });
  }



 // Affichage du menu avatar
  AvatarMenu.prototype.launchAvatarMenuScene = function() {
    this.displayMenuBackground();

    setTimeout(this.displayObjects.bind(this), 100); // Affichage des objets possédés
    //setTimeout(this.displayButtons.bind(this), 100); // Affichage boutons
    setTimeout(this.displayAvatar.bind(this), 100); // Affichage avatar

    // Ajout du listener via la fonction
    canvas.addEventListener('click', avatarMenuClickHandler);
  }


  // Affichage du background
  AvatarMenu.prototype.displayMenuBackground = function() {
    var background = new Image();
    background.onload = function() {
        context.drawImage(background, 0, 0, 1400, 700);
     }
     background.src = 'assets/imgs/menus/AvatarMenu.png';
  }


  // Affichage des objets
  AvatarMenu.prototype.displayObjects = function() {
    this.player.ownedObjects.forEach(object => {
      // // Cadre de l'objet
      // context.beginPath();
      // context.rect(object.x1, object.y1, object.width, object.height);
      // context.stroke();


      // Iconne si selectionné
      if(this.isEquiped(object.name)) {
        var goodIcon = new Image();
        goodIcon.onload = function() {
          context.drawImage(goodIcon,  object.x1+67, object.y1-20, 30, 30); // Affichage svg iconne check
        }
        goodIcon.src = 'assets/imgs/good.svg';
      }

      // Image de l'objet
      let objectImage = new Image();
      objectImage.onload = function() {
          context.drawImage(objectImage, 25, typeToSy(object.type), 200, 200, object.x1, object.y1, object.width, object.height);
       }
       objectImage.src = 'assets/imgs/avatar/'+object.name+'.png';
    });
  }


  // Affichage de l'avatar
  AvatarMenu.prototype.displayAvatar = function() {
    var imagesCount = 1; // Nombre d'images à charger
    var imagesLoaded = 0; // Images chargées

    // Stockage avatar
    var avatarImage = new Image();
    var avatar = this.player.defaultAvatar;
    avatarImage.src = 'assets/imgs/avatar/avatar_'+avatar+'.png';


    // Stockage des images et leur path si objet selectionné :
    // Tete
    var headImage = new Image();
    var head = this.player.equipedObjects.head;
    if(head != null) {
      imagesCount ++;
      headImage.src = 'assets/imgs/avatar/'+head+'.png';
    }

    // Corps
    var bodyImage = new Image();
    var body = this.player.equipedObjects.body;
    if(body != null) {
      imagesCount ++;
      bodyImage.src = 'assets/imgs/avatar/'+body+'.png';
    }

    // Mains
    var handsImage = new Image();
    var hands = this.player.equipedObjects.hands;
    if(hands != null) {
      imagesCount ++;
      handsImage.src = 'assets/imgs/avatar/'+hands+'.png';
    }

    // Jambes
    var legsImage = new Image();
    var legs = this.player.equipedObjects.legs;
    if(legs != null) {
      imagesCount ++;
      legsImage.src = 'assets/imgs/avatar/'+legs+'.png';
    }

    // Pieds
    var feetImage = new Image();
    var feet = this.player.equipedObjects.feet;
    if(feet != null) {
      imagesCount ++;
      feetImage.src = 'assets/imgs/avatar/'+feet+'.png';
    }

    // Listes des images et des parties de l'avatar
    var images = [avatarImage, headImage, bodyImage, handsImage, legsImage, feetImage];
    var parts = [head, body, hands, legs, feet];

    // Incrémentation du nb d'images chargées et affichage quand complet
    for(var i=0; i<images.length; i++){
      images[i].onload = function(){
        imagesLoaded++;
        if(imagesLoaded == imagesCount){
          context.drawImage(images[0], avatarImageCoords.x, avatarImageCoords.y, avatarImageCoords.width, avatarImageCoords.height);
          for(var j=0; j<parts.length; j++){
            context.drawImage(images[j+1], avatarImageCoords.x, avatarImageCoords.y, avatarImageCoords.width, avatarImageCoords.height);
          }
        }
      }
    }

  }


  // Vérifie si l'objet est équipé
  AvatarMenu.prototype.isEquiped = function(name) {
    let equiped = this.player.equipedObjects;
      if(equiped.head == name || equiped.body == name ||
        equiped.hands == name || equiped.legs == name ||
        equiped.feet == name) {
          return true;
      } else {
        return false;
      }
  }


  // Affichage des boutons
  AvatarMenu.prototype.displayButtons = function() {
    // Affichage iconne de fermeture
     context.beginPath();
     context.arc(crossIconCoords.x, crossIconCoords.y, crossIconCoords.radius, 0, 2 * Math.PI, false);
     context.fillStyle = 'grey';
     context.fill();
  }


  // Fermeture de la fenêtre
  AvatarMenu.prototype.closeAvatarMenuScene = function() {
    this.removeClickListener();
    controller.displayExplorationMenu();
  }



  // Suppression du listener de click sur le menu avatar
  AvatarMenu.prototype.removeClickListener = function() {
    canvas.removeEventListener('click', avatarMenuClickHandler);
  }
}
