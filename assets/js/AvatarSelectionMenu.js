/*
  Classe controllant le menu d'options du jeu.

  Elle gère :
  - L'affichage des boutons interactifs
  - L'affichage des avatars
  - Le stockage de l'avatar selectionné
  - Le listener
*/

function AvatarSelectionMenu() {

  // Coordonnées image avatar 1
  const avatar1ImageCoords = {
    x: 200,
    y: 150,
    width: 250,
    height: 470
  }


  // Coordonnées image avatar 2
  const avatar2ImageCoords = {
    x: 950,
    y: 150,
    width: 250,
    height: 470
  }


  // Fonction listener de click sur le canvas (pas de lambda expression pour pouvoir la remove apres)
  var selectionClickHandler =  function(e) {

    var it = controller.avatarSelectionMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };

    // Vérifie interactions avec avatar 1
    if (isSquareIntersect(mouse, avatar1ImageCoords)) {
      it.removeClickListener();
      controller.player.defaultAvatar = 1;
      controller.displayUsernameMenu();
    }

    // Vérifie interactions avec avatar 2
    if (isSquareIntersect(mouse, avatar2ImageCoords)) {
      it.removeClickListener();
      controller.player.defaultAvatar = 2;
      controller.displayUsernameMenu();
    }
  }


  // Fonction d'affichage de la scène pour le menu de lancement
  AvatarSelectionMenu.prototype.launchAvatarSelectionMenuScene = function() {
    this.displayMenuBackground();
    //setTimeout(this.displayImages.bind(this), 100);

    // Ajout du listener via la fonction
    canvas.addEventListener('click', selectionClickHandler);
  }


  // Affichage du background
  AvatarSelectionMenu.prototype.displayMenuBackground = function() {
    var background = new Image();
    background.onload = function() {
        context.drawImage(background, 0, 0, 1400, 700);
     }
     background.src = 'assets/imgs/menus/AvatarSelectionMenu.png';
  }


  // Suppression du listener
  AvatarSelectionMenu.prototype.removeClickListener = function() {
    canvas.removeEventListener('click', selectionClickHandler);
  }


  // Affichage des avatars
  AvatarSelectionMenu.prototype.displayImages = function() {
    // Affichage avatar 1
    context.beginPath();
    context.rect(avatar1ImageCoords.x, avatar1ImageCoords.y, avatar1ImageCoords.width, avatar1ImageCoords.height);
    context.stroke();

     // Affichage avatar 2
     context.beginPath();
     context.rect(avatar2ImageCoords.x, avatar2ImageCoords.y, avatar2ImageCoords.width, avatar2ImageCoords.height);
     context.stroke();
  }

}
