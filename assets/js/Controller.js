/*
  Classe coordonnant les interactions du système et le dialogue entre classes.

  Elle contient :
  - Le joueur et ses paramètres (objet)
  - Les menus (objets) définis statiquement ou dynamiquement selon les besoins

  Elle gère :
  - La réinitialisation de l'affichage sur le canvas
  - Le lancement de l'affichage de chacun des menus
  - La mise à jour des stats du joueur après une action
  - La sauvegarde auto (interne) du profil du joueur
  - La suppression de la sauvegarde interne
  - Le chargement de la sauvegarde interne
  - L'import du profil du joueur
  - L'export du profil du joueur
*/

function Controller() {

  this.player = new Player('Louis'); // Joueur associé au controleur (objet)
  this.launchMenu = new LaunchMenu(); // Menu de lancement (objet)
  this.loadingMenu = new LoadingMenu(); // Menu de chargement (objet)
  this.introMenu = new IntroMenu(); // Menu d'introduction (objet)
  this.modeMenu = new ModeMenu(); // Menu de choix du mode (objet)
  this.avatarSelectionMenu = new AvatarSelectionMenu(); // Menu de choix de l'avatar (objet)
  this.usernameMenu = new UsernameMenu(); // Menu de choix du pseudo (objet)
  this.explorationMenu = new ExplorationMenu(this.player); // Menu d'exploration (objet)
  this.optionsMenu = new OptionsMenu(this.player); // Menu options
  this.avatarMenu = new AvatarMenu(this.player); // Menu avatar
  this.shopMenu = new ShopMenu(this.player); // Menu boutique
  this.pauseMenu = null; // Menu pause
  this.levelMenu = null // Menu pré-niveau (objet)
  this.level = null; // Objet gérant le niveau (questions ...)
  this.levelEndMenu = null; // Menu post-niveau (objet)
  //this.music = new Sound("assets/sounds/music.mp3");


  // Affichage du menu de lancement
  Controller.prototype.displayLaunchMenu = function() {
    this.clearScene();  // Réinitialisation affichage
    this.launchMenu.launchLaunchMenuScene(); // Lancement de la scène du menu de lancement
  }


  // Affichage du menu de chargement
  Controller.prototype.displayLoadingMenu = function() {
    this.clearScene();  // Réinitialisation affichage
    this.loadingMenu.launchLoadingMenuScene(); // Lancement de la scène du menu de chargement
  }


  // Affichage du menu d'introduction
  Controller.prototype.displayIntroMenu = function() {
    this.clearScene();  // Réinitialisation affichage
    this.introMenu.launchIntroMenuScene(); // Lancement de la scène du menu
  }


  // Affichage du menu de choix du mode
  Controller.prototype.displayModeMenu = function() {
    this.clearScene();  // Réinitialisation affichage
    this.modeMenu.launchModeMenuScene(); // Lancement de la scène du menu
  }


  // Affichage du menu de choix de l'avatar
  Controller.prototype.displayAvatarSelectionMenu = function() {
    this.clearScene();  // Réinitialisation affichage
    this.avatarSelectionMenu.launchAvatarSelectionMenuScene(); // Lancement de la scène du menu
  }


  // Affichage du menu de choix du pseudo
  Controller.prototype.displayUsernameMenu = function() {
    this.usernameMenu.launchUsernameMenuScene(); // Lancement de la scène du menu
  }


  // Affichage du menu d'exploration
  Controller.prototype.displayExplorationMenu = function() {
    this.clearScene();  // Réinitialisation affichage
    this.explorationMenu.launchExplorationScene(); // Lancement de la scène du menu d'exploration
  }


  // Affichage du menu pré-niveau
  Controller.prototype.displayLevelMenu = function(world, level) {
    this.explorationMenu.removeClickListener(); // Remove le listener de click du menu exploration

    this.levelMenu = new LevelMenu(world, level);
    this.levelMenu.launchLevelMenuScene();
  }


  // Affichage du menu options
  Controller.prototype.displayOptionsMenu = function() {
    this.explorationMenu.removeClickListener(); // Remove le listener de click du menu exploration
    this.clearScene();  // Réinitialisation affichage

    this.optionsMenu.launchOptionsMenuScene();
  }


  // Affichage du menu avatar
  Controller.prototype.displayAvatarMenu = function() {
    this.explorationMenu.removeClickListener(); // Remove le listener de click du menu exploration
    this.clearScene();  // Réinitialisation affichage

    this.avatarMenu.launchAvatarMenuScene();
  }


  // Affichage du menu boutique
  Controller.prototype.displayShopMenu = function() {
    this.explorationMenu.removeClickListener(); // Remove le listener de click du menu exploration
    this.clearScene();  // Réinitialisation affichage

    this.shopMenu.launchShopMenuScene();
  }


  // Lancement du niveau
  Controller.prototype.launchLevel = function(world, level) {
    this.clearScene(); // Réinitialisation affichage
    this.levelMenu.removeClickListener(); // Remove le listener de click du menu pré-niveau

    this.level = new Level(world, level);
    this.level.launchLevelScene(); // Lancement de la scène du niveau
  }


  // Reprise du niveau
  Controller.prototype.resumeLevel = function(world, level, question, answer, answerDisplay, count) {
    this.clearScene(); // Réinitialisation affichage

    this.level = new Level(world, level);
    this.level.resumeLevelScene(question, answer, answerDisplay, count); // Lancement de la scène du niveau
  }


  // Reprise du niveau
  Controller.prototype.resumeProblemLevel = function(world, level, problem, answer, count) {
    this.clearScene(); // Réinitialisation affichage

    this.level = new Level(world, level);
    this.level.resumeProblemLevelScene(problem, answer, count); // Lancement de la scène du niveau
  }


  // Reprise du niveau
  Controller.prototype.resumeSpecialLevel = function(world, level, question, answer, answerDisplay, count, time) {
    this.clearScene(); // Réinitialisation affichage

    this.level = new Level(world, level);
    this.level.resumeSpecialLevelScene(question, answer, answerDisplay, count, time); // Lancement de la scène du niveau
  }


  // Reprise du niveau
  Controller.prototype.resumeBossLevel = function(world, level, question, answer, answerDisplay, playerLives, bossLives) {
    this.clearScene(); // Réinitialisation affichage

    this.level = new Level(world, level);
    this.level.resumeBossLevelScene(question, answer, answerDisplay, playerLives, bossLives); // Lancement de la scène du niveau
  }


  // Fonction nettoyant tout l'affichage de la scene
  Controller.prototype.clearScene = function() {
    context.clearRect(0, 0, canvas.width, canvas.height);
  }


  // Affichage de la fenêtre pause
  Controller.prototype.displayPauseMenu = function(level) {

    this.level.removeInputBox();
    this.level.removeListeners();

    this.pauseMenu = new PauseMenu(level);
    this.pauseMenu.launchPauseMenuScene();
  }


  // Lancement de la fin du niveau (calcul scores et menu de fin)
  Controller.prototype.launchLevelEnd = function(level) {
    // Mise à jour des données joueur et niveau
    this.updateData(level);
    this.saveProfile();

    this.level.removeInputBox();
    this.level.removeListeners();

    this.levelEndMenu = new LevelEndMenu(level);

    if(level.explorationLevel.type == 'boss') {
      if(level.bossLives == 0) {
        this.levelEndMenu.launchLevelEndScene();
      } else {
        this.levelEndMenu.launchLostLevelEndScene();
      }

    } else {
      if(level.wins >= 3) {
        this.levelEndMenu.launchLevelEndScene();
      } else {
        this.levelEndMenu.launchLostLevelEndScene();
      }
    }



  }


  // Mise à jour des données joueur
  Controller.prototype.updateData = function(level) {
    // Etoiles obtenues selon score
    let starsObtained = null;
    if(level.explorationLevel.type == 'boss') {
      if(level.bossLives == 0) {
        starsObtained = 3;
      } else {
        starsObtained = 0;
      }

    } else {
      if(level.wins == 10) {
        starsObtained = 3;
      } else if(level.wins >= 6) {
        starsObtained = 2;
      } else if(level.wins >= 3) {
        starsObtained = 1;
      } else {
        starsObtained = 0;
      }
    }


    // Mise à jour des étoiles du niveau
    if(level.explorationLevel.stars < starsObtained) {
      level.explorationLevel.stars = starsObtained;
    }


    // Gain d'experience du joueur en fonction du nombre d'étoiles obtenues
    let experienceGain = level.explorationLevel.experienceGiven * starsObtained;

    if(this.player.experience + experienceGain >= 90) {
      let exp = this.player.experience + experienceGain; // Experience totale
      let levelGain = Math.floor(exp / 90); // Gain de niveau partie entière sur 90 (exp necessaire pour un niveau)
      this.player.level += levelGain;

      experienceGain = exp % 90; // Experience restante apres gain de niveau (modulo)
      this.player.experience = experienceGain;

    } else {
      this.player.experience += experienceGain;
    }


    // Gain d'argent du joueur en fonction du nombre d'étoiles obtenues
    let moneyGain = level.explorationLevel.moneyGiven * starsObtained;
    this.player.money += moneyGain;


    // Mise à jour des pièces de vaisseau obtenues si le niveau en propose
    if(level.explorationLevel.hasShipPart && level.explorationLevel.type == 'special' && starsObtained >= 1) {
      level.world.shipParts += 1;
      // Vérifie si vaisseau complet
      if(level.world.shipParts == 4) {
        level.world.shipState = 'complete';
      }


      //Sinon si le niveau est normal et que le joueur a eu 3 étoiles
    } else if(level.explorationLevel.hasShipPart && starsObtained == 3) {
      level.world.shipParts += 1;
      // Vérifie si vaisseau complet
      if(level.world.shipParts == 4) {
        level.world.shipState = 'complete';
      }
    }


    // Deblocage du niveau suivant si une étoile ou plus
    if(level.explorationLevel.id != 10 && level.explorationLevel.isOptionnal == false) {
      if(starsObtained >= 1 && level.world.levels[level.explorationLevel.id].state == 'locked') {
        level.world.levels[level.explorationLevel.id].state = 'unlocked';
      }
    } else if(level.explorationLevel.isOptionnal == true) {
      if(starsObtained >= 1 && level.world.levels[level.explorationLevel.nextLevel-1].state == 'locked') {
        level.world.levels[level.explorationLevel.nextLevel-1].state = 'unlocked';
      }
    }


    // Deblocage du niveau optionnel suivant si une étoile ou plus
    if(level.explorationLevel.id != 10 && level.explorationLevel.nextLevel != null && level.explorationLevel.isOptionnal == false) {
      if(starsObtained >= 1 && level.world.levels[level.explorationLevel.nextLevel-1].state == 'locked') {
        level.world.levels[level.explorationLevel.nextLevel-1].state = 'unlocked';
      }
    }


    // Deblocage du monde suivant si conditions réunies (vaisseau complet et boss vaincu)
    if(level.world.shipState == 'complete' && level.world.levels[9].stars == 3) {
      this.explorationMenu.worlds[this.explorationMenu.currentWorld+1].state = 'unlocked';
    }
  }



  // Sauvegarde du profil sur le localStorage
  Controller.prototype.saveProfile = function() {
    // Joueur
    localStorage.setItem('username',JSON.stringify(this.player.username));
    localStorage.setItem('defaultAvatar',JSON.stringify(this.player.defaultAvatar));
    localStorage.setItem('level',JSON.stringify(this.player.level));
    localStorage.setItem('experience',JSON.stringify(this.player.experience));
    localStorage.setItem('money',JSON.stringify(this.player.money));
    localStorage.setItem('shipParts',JSON.stringify(this.player.shipParts));
    localStorage.setItem('difficulty',JSON.stringify(this.player.difficulty));

    // Objets
    localStorage.setItem('ownedObjects',JSON.stringify(this.player.ownedObjects));
    localStorage.setItem('equipedHead',JSON.stringify(this.player.equipedObjects.head));
    localStorage.setItem('equipedBody',JSON.stringify(this.player.equipedObjects.body));
    localStorage.setItem('equipedhands',JSON.stringify(this.player.equipedObjects.hands));
    localStorage.setItem('equipedLegs',JSON.stringify(this.player.equipedObjects.legs));
    localStorage.setItem('equipedFeet',JSON.stringify(this.player.equipedObjects.feet));
    localStorage.setItem('objectsList',JSON.stringify(this.shopMenu.objectsList));

    // Mondes et Niveaux
    localStorage.setItem('worlds',JSON.stringify(this.explorationMenu.worlds));
    localStorage.setItem('levels-world1',JSON.stringify(this.explorationMenu.worlds[0].levels));
    localStorage.setItem('levels-world2',JSON.stringify(this.explorationMenu.worlds[1].levels));
    localStorage.setItem('levels-world3',JSON.stringify(this.explorationMenu.worlds[2].levels));
    localStorage.setItem('levels-world4',JSON.stringify(this.explorationMenu.worlds[3].levels));
    localStorage.setItem('levels-world5',JSON.stringify(this.explorationMenu.worlds[4].levels));
    localStorage.setItem('levels-world6',JSON.stringify(this.explorationMenu.worlds[5].levels));
    localStorage.setItem('levels-world7',JSON.stringify(this.explorationMenu.worlds[6].levels));
    localStorage.setItem('levels-world8',JSON.stringify(this.explorationMenu.worlds[7].levels));
    localStorage.setItem('levels-world9',JSON.stringify(this.explorationMenu.worlds[8].levels));
    localStorage.setItem('levels-world10',JSON.stringify(this.explorationMenu.worlds[9].levels));
    localStorage.setItem('currentWorld',JSON.stringify(this.explorationMenu.currentWorld));
  }


  // Suppression du profil sur le localStorage
  Controller.prototype.removeProfile = function() {
    localStorage.clear();

    var notif = new Notification('Suppression sauvegarde interne ...', 410);
    notif.displayNotification();

    setTimeout(document.location.reload.bind(document.location), 2000);
  }


  // Chargement du profil depuis le localStorage
  Controller.prototype.loadProfile = function() {

    // Si username définit : il y a une sauvegarde
    if(localStorage.getItem('username') != undefined) {
      // Joueur
      this.player.username = JSON.parse(localStorage.getItem('username'));
      this.player.defaultAvatar = JSON.parse(localStorage.getItem('defaultAvatar'));
      this.player.level = JSON.parse(localStorage.getItem('level'));
      this.player.experience = JSON.parse(localStorage.getItem('experience'));
      this.player.money = JSON.parse(localStorage.getItem('money'));
      this.player.shipParts = JSON.parse(localStorage.getItem('shipParts'));
      this.player.difficulty = JSON.parse(localStorage.getItem('difficulty'));

      // Objets
      this.player.ownedObjects = JSON.parse(localStorage.getItem('ownedObjects'));
      this.player.equipedObjects.head = JSON.parse(localStorage.getItem('equipedHead'));
      this.player.equipedObjects.body = JSON.parse(localStorage.getItem('equipedBody'));
      this.player.equipedObjects.hands = JSON.parse(localStorage.getItem('equipedHands'));
      this.player.equipedObjects.legs = JSON.parse(localStorage.getItem('equipedLegs'));
      this.player.equipedObjects.feet = JSON.parse(localStorage.getItem('equipedFeet'));
      this.shopMenu.objectsList = JSON.parse(localStorage.getItem('objectsList'));;

      // Mondes et Niveaux
      this.explorationMenu.worlds = JSON.parse(localStorage.getItem('worlds'));
      this.explorationMenu.worlds[0].levels = JSON.parse(localStorage.getItem('levels-world1'));
      this.explorationMenu.worlds[1].levels = JSON.parse(localStorage.getItem('levels-world2'));
      this.explorationMenu.worlds[2].levels = JSON.parse(localStorage.getItem('levels-world3'));
      this.explorationMenu.worlds[3].levels = JSON.parse(localStorage.getItem('levels-world4'));
      this.explorationMenu.worlds[4].levels = JSON.parse(localStorage.getItem('levels-world5'));
      this.explorationMenu.worlds[5].levels = JSON.parse(localStorage.getItem('levels-world6'));
      this.explorationMenu.worlds[6].levels = JSON.parse(localStorage.getItem('levels-world7'));
      this.explorationMenu.worlds[7].levels = JSON.parse(localStorage.getItem('levels-world8'));
      this.explorationMenu.worlds[8].levels = JSON.parse(localStorage.getItem('levels-world9'));
      this.explorationMenu.worlds[9].levels = JSON.parse(localStorage.getItem('levels-world10'));
      this.explorationMenu.currentWorld = JSON.parse(localStorage.getItem('currentWorld'));
    }
  }


  // Export du profil
  Controller.prototype.exportProfile = function() {
    var data = {
      username: this.player.username,
      defaultAvatar: this.player.defaultAvatar,
      level: this.player.level,
      experience: this.player.experience,
      money: this.player.money,
      shipParts: this.player.shipParts,
      difficulty: this.player.difficulty,
      ownedObjects: this.player.ownedObjects,
      equipedObjects: this.player.equipedObjects,
      worlds: this.explorationMenu.worlds,
      levels1: this.explorationMenu.worlds[0].levels,
      levels2: this.explorationMenu.worlds[1].levels,
      levels3: this.explorationMenu.worlds[2].levels,
      levels4: this.explorationMenu.worlds[3].levels,
      levels5: this.explorationMenu.worlds[4].levels,
      levels6: this.explorationMenu.worlds[5].levels,
      levels7: this.explorationMenu.worlds[6].levels,
      levels8: this.explorationMenu.worlds[7].levels,
      levels9: this.explorationMenu.worlds[8].levels,
      levels10: this.explorationMenu.worlds[9].levels,
      currentWorld: this.explorationMenu.currentWorld,
      objectsList: this.shopMenu.objectsList
    };

    var json = JSON.stringify(data);
    var blob = new Blob([json], {type: "application/json"});
    saveAs(blob, "profil_"+this.player.username+".json");
  }



  // Import du profil
  Controller.prototype.importProfile = function(data) {
    // Joueur
    this.player.username = data.username;
    this.player.defaultAvatar = data.defaultAvatar;
    this.player.level = data.level;
    this.player.experience = data.experience;
    this.player.money = data.money;
    this.player.shipParts = data.shipParts;
    this.player.difficulty = data.difficulty;

    // Objets
    this.player.ownedObjects = data.ownedObjects;
    this.player.equipedObjects = data.equipedObjects;
    this.shopMenu.objectsList = data.objectsList;

    // Mondes et Niveaux
    this.explorationMenu.worlds = data.worlds;
    this.explorationMenu.worlds[0].levels = data.levels1;
    this.explorationMenu.worlds[1].levels = data.levels2;
    this.explorationMenu.worlds[2].levels = data.levels3;
    this.explorationMenu.worlds[3].levels = data.levels4;
    this.explorationMenu.worlds[4].levels = data.levels5;
    this.explorationMenu.worlds[5].levels = data.levels6;
    this.explorationMenu.worlds[6].levels = data.levels7;
    this.explorationMenu.worlds[7].levels = data.levels8;
    this.explorationMenu.worlds[8].levels = data.levels9;
    this.explorationMenu.worlds[9].levels = data.levels10;
    this.explorationMenu.currentWorld = data.currentWorld;
  }


  // Masque le selecteur de fichiers
  Controller.prototype.hideFileSelectors = function() {
    let input1 = document.getElementById("inputFileLabel1");
    input1.style.visibility = 'hidden';

    let input2 = document.getElementById("inputFileLabel2");
    input2.style.visibility = 'hidden';
  }

}
