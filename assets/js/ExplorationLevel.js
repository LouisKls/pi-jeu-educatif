/*
  Classe correspondant à un niveau du menu d'exploration. Elle gère :
  - Son affichage via un cercle (coordonnées, rayon, couleur)
  - Ses caractéristiques (id, état, type, nombre d'étoiles remportées)
  - Les récompenses qu'il offre (experience, argent, pièce de vaisseau, bonus)

  Etat :
  - Il peut être égal à 'locked' ou 'unlocked' selon la progression du joueur
  - Le niveau n'est accessible que s'il est unlocked

  Type :
  - Il peut-être égal à 'normal', 'special' ou 'boss' selon le type du niveau
  - Le type influe sur les questions et/ou la forme du défi

  Récompenses :
  - Leur valeur est multipliée par le nombre d'étoiles remportées (sauf pièce et bonus)
*/


function ExplorationLevel(x, y, color, id, state, type, exp, money, part, optionnal, prev, next) {
  this.x = x; // Position en x du cercle
  this.y = y; // Position en y du cercle
  this.radius = 35; // Rayon du cercle
  this.color = color; // Couleur du cercle
  this.id = id; // Id ou numéro du niveau
  this.state = state; // Etat du niveau (bloqué, débloqué, terminé)
  this.type = type; // Type du niveau (normal, spécial, boss)
  this.stars = 0; // Nombre d'étoiles obtenues
  this.experienceGiven = exp; // Experience gagnée en réussissant le niveau (fois le nombre d'étoiles remportées)
  this.moneyGiven = money; // Argent gagné en réussissant le niveau (fois le nombre d'étoiles remportées)
  this.hasShipPart = part; // Booléen si le niveau contient une pièce de vaisseau
  this.isOptionnal = optionnal; // Booléen si le niveau est un chemin optionnel
  this.previousLevel = prev; // Id du niveau précédent si optionnel, null sinon
  this.nextLevel = next; // Id du niveau suivant si optionnel, null sinon

}
