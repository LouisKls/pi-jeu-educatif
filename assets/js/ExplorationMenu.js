/*
  Classe controlant le menu d'exploration (choix des niveaux du monde).

  Elle contient :
  - Le joueur et ses informations en paramètre
  - L'id du monde actuel dans lequel le joueur progresse
  - Une liste complète des mondes disponibles (objets)
  - Une liste complète des niveaux pour chacun des mondes (objets)

  Elle gère :
  - L'affichage des stats du joueur (argent, niveau, experience, pièces de vaisseau)
  - L'affichage miniature de l'avatar du joueur
  - L'affichage des niveaux du monde actuel et leurs iconnes
  - L'affichage des boutons interactifs
*/

function ExplorationMenu(player) {

  this.player = player; // Objet joueur
  this.currentWorld = 0;  // Monde actuellement affiché dans lequel le joueur progresse


  // Liste complète des mondes (objets)
  this.worlds = [
    new ExplorationWorld(0,'Planète Terre', 'Additions'),
    new ExplorationWorld(1,'Lune', 'Soustractions'),
    new ExplorationWorld(2,'Mars','Complément à 10'),
    new ExplorationWorld(3,'Jupiter','Doubles'),
    new ExplorationWorld(4,'Saturne','Additions à trou'),
    new ExplorationWorld(5,'Uranus','Multiplications (tables 3 à 5)'),
    new ExplorationWorld(6,'Neptune','Multiplications (tables 6 à 9)'),
    new ExplorationWorld(7,'Pluton','Soustractions à trou'),
    new ExplorationWorld(8,'Vénus','Problèmes simples'),
    new ExplorationWorld(9,'Mercure','Problèmes complexes')
 ];


  // Codage en dur de l'affichage des niveaux pour chaque monde afin de les disposer de facon spécifique (prévision pour partie graphique)
  // Niveaux du premier monde
  this.worlds[0].levels = [
    new ExplorationLevel(105,280,'green',1,'unlocked','normal', 10, 50, false, false, null, null), // Le premier niveau est forcément débloqué
    new ExplorationLevel(220,380,'green',2,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(356,478,'green',3,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(511,555,'green',4,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(673,479,'green',5,'locked','normal', 10, 50, false, false, null, 11),
    new ExplorationLevel(764,350,'green',6,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(924,280,'green',7,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(1074,350,'green',8,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(1192,446,'green',9,'locked','normal', 10, 50, true, false, 11, null),
    new ExplorationLevel(1322,385,'green',10,'locked','boss', 50, 200, false, false, null, null),
    new ExplorationLevel(936,542,'green',11,'locked','boss', 50, 200, true, true, 5, 9)
  ];


  // Niveaux du second monde
  this.worlds[1].levels = [
    new ExplorationLevel(143,498,'green',1,'unlocked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(256,382,'green',2,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(406,463,'green',3,'locked','normal', 10, 50, false, false, null, 11),
    new ExplorationLevel(550,564,'green',4,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(669,460,'green',5,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(828,415,'green',6,'locked','normal', 10, 50, false, false, 11, null),
    new ExplorationLevel(967,495,'green',7,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(1113,432,'green',8,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(1247,499,'green',9,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(1322,385,'green',10,'locked','boss', 50, 200, false, false, null, null),
    new ExplorationLevel(599,327,'green',11,'locked','boss', 50, 200, true, true, 3, 6)
  ];


  // Niveaux du troisième monde
  this.worlds[2].levels = [
    new ExplorationLevel(160,280,'green',1,'unlocked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(220,380,'green',2,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(356,478,'green',3,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(511,555,'green',4,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(673,479,'green',5,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(764,350,'green',6,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(924,280,'green',7,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(1074,350,'green',8,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(1192,446,'green',9,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(1322,385,'green',10,'locked','boss', 50, 200, false, false, null, null)
  ];


  // Niveaux du quatrième monde
  this.worlds[3].levels = [
    new ExplorationLevel(160,280,'green',1,'unlocked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(220,380,'green',2,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(356,478,'green',3,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(511,555,'green',4,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(673,479,'green',5,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(764,350,'green',6,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(924,280,'green',7,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(1074,350,'green',8,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(1192,446,'green',9,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(1322,385,'green',10,'locked','boss', 50, 200, false, false, null, null)
  ];


  // Niveaux du cinquième monde
  this.worlds[4].levels = [
    new ExplorationLevel(160,280,'green',1,'unlocked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(220,380,'green',2,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(356,478,'green',3,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(511,555,'green',4,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(673,479,'green',5,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(764,350,'green',6,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(924,280,'green',7,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(1074,350,'green',8,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(1192,446,'green',9,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(1322,385,'green',10,'locked','boss', 50, 200, false, false, null, null)
  ];


  // Niveaux du sixième monde
  this.worlds[5].levels = [
    new ExplorationLevel(160,280,'green',1,'unlocked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(220,380,'green',2,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(356,478,'green',3,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(511,555,'green',4,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(673,479,'green',5,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(764,350,'green',6,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(924,280,'green',7,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(1074,350,'green',8,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(1192,446,'green',9,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(1322,385,'green',10,'locked','boss', 50, 200, false, false, null, null)
  ];


  // Niveaux du septième monde
  this.worlds[6].levels = [
    new ExplorationLevel(160,280,'green',1,'unlocked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(220,380,'green',2,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(356,478,'green',3,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(511,555,'green',4,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(673,479,'green',5,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(764,350,'green',6,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(924,280,'green',7,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(1074,350,'green',8,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(1192,446,'green',9,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(1322,385,'green',10,'locked','boss', 50, 200, false, false, null, null)
  ];


  // Niveaux du huitième monde
  this.worlds[7].levels = [
    new ExplorationLevel(160,280,'green',1,'unlocked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(220,380,'green',2,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(356,478,'green',3,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(511,555,'green',4,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(673,479,'green',5,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(764,350,'green',6,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(924,280,'green',7,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(1074,350,'green',8,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(1192,446,'green',9,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(1322,385,'green',10,'locked','boss', 50, 200, false, false, null, null)
  ];


  // Niveaux du neuvième monde
  this.worlds[8].levels = [
    new ExplorationLevel(160,280,'green',1,'unlocked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(220,380,'green',2,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(356,478,'green',3,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(511,555,'green',4,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(673,479,'green',5,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(764,350,'green',6,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(924,280,'green',7,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(1074,350,'green',8,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(1192,446,'green',9,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(1322,385,'green',10,'locked','boss', 50, 200, false, false, null, null)
  ];


  // Niveaux du dixième monde
  this.worlds[9].levels = [
    new ExplorationLevel(160,280,'green',1,'unlocked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(220,380,'green',2,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(356,478,'green',3,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(511,555,'green',4,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(673,479,'green',5,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(764,350,'green',6,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(924,280,'green',7,'locked','special', 20, 100, true, false, null, null),
    new ExplorationLevel(1074,350,'green',8,'locked','normal', 10, 50, false, false, null, null),
    new ExplorationLevel(1192,446,'green',9,'locked','normal', 10, 50, true, false, null, null),
    new ExplorationLevel(1322,385,'green',10,'locked','boss', 50, 200, false, false, null, null)
  ];


  // Coordonnées iconne avatar
  const avatarIconCoords = {
    x: (canvas.width/2) - 45,
    y: 42,
    width: 90,
    height: 89
  }


  // Coordonnées nom du monde
  const worldNameCoords = {
    x: (canvas.width/2) - 60,
    y: canvas.height - 30
  }


  // Coordonnées iconne flèche droite changement de monde
  const rightArrowIconCoords = {
    x: canvas.width - 75,
    y: canvas.height/2 - 61,
    radius: 30
  }


  // Coordonnées iconne flèche gauche changement de monde
  const leftArrowIconCoords = {
    x: 67,
    y: canvas.height/2 - 61,
    radius: 30
  }


  // Coordonnées iconne options
  const optionsIconCoords = {
    x: canvas.width - 106,
    y: 81,
    radius: 50
  }


  // Coordonnées iconne d'arrêt
  const offIconCoords = {
    x: 99,
    y: 81,
    radius: 50
  }


  // Coordonnées iconne boutique
  const shopIconCoords = {
    x: canvas.width - 106,
    y: canvas.height - 69,
    radius: 50
  }


  // Coordonnées label pièces vaisseau
  const shipPartsLabelCoords = {
    x: (canvas.width/2) - 338,
    y: 91
  }


  // Coordonnées label argent
  const moneyLabelCoords = {
    x: canvas.width - 370,
    y: 91
  }


  // Coordonnées barre d'experience
  const experienceBarCoords = {
    x: (canvas.width/2) - 48,
    y: 169,
    height: 30
  }



  // Fonction listener de click sur le canvas (pas de lambda expression pour pouvoir la remove apres)
  var explorationClickHandler =  function(e) {

    var it = controller.explorationMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };

    // Verifie interactions avec niveaux (cercles)
    it.worlds[it.currentWorld].levels.forEach(level => {
      if (isCircleIntersect(mouse, level) && level.state != 'locked') { // Si le niveau est accessible
        controller.displayLevelMenu(it.worlds[it.currentWorld],it.worlds[it.currentWorld].levels[level.id-1]);
      } else if(isCircleIntersect(mouse, level) && level.state == 'locked') { // Si le niveau est bloqué
        var notif = new Notification('Termine les niveaux precedents pour acceder a ce niveau !', 200);
        notif.displayNotification();
      }
    });

    // Vérifie interactions avec l'iconne d'avatar
    if (isSquareIntersect(mouse, avatarIconCoords)) {
      controller.displayAvatarMenu();
    }

    // Vérifie interactions avec bouton d'arrêt
    if (isCircleIntersect(mouse, offIconCoords)) {
      document.location.reload();
    }

    // Vérifie interactions avec bouton options
    if (isCircleIntersect(mouse, optionsIconCoords)) {
      controller.displayOptionsMenu();
    }

    // Vérifie interactions avec bouton de la boutique
    if (isCircleIntersect(mouse, shopIconCoords)) {
      controller.displayShopMenu();
    }

    // Vérifie interactions avec flèche de changement de monde à droite
    if (isCircleIntersect(mouse, rightArrowIconCoords)) {
      if(it.worlds[it.currentWorld+1].state == 'unlocked') {
        it.currentWorld ++;
        it.removeClickListener();
        controller.displayExplorationMenu();
      } else if(it.worlds[it.currentWorld].levels[9].stars == 0){
        var notif = new Notification("Tu dois battre le boss du monde avant de voyager", 250);
        notif.displayNotification();
      } else {
        var notif = new Notification("Tu dois réparer le vaisseau avant de voyager", 250);
        notif.displayNotification();
      }
    }

    // Vérifie interactions avec flèche de changement de monde à gauche
    if (isCircleIntersect(mouse, leftArrowIconCoords)) {
      if(it.currentWorld > 0) {
        it.currentWorld --;
        it.removeClickListener();
        controller.displayExplorationMenu();
      }
    }

  }



  // Fonction d'affichage de la scène pour le menu exploration (initialisation)
  ExplorationMenu.prototype.launchExplorationScene = function() {
    this.displayMenuBackground();
    setTimeout(this.displayPlayerStats.bind(this),50);
     // setTimeout(this.displayLevels.bind(this),1000);
     // setTimeout(this.displayMenuButtons.bind(this),1000);
    setTimeout(this.displayAvatar.bind(this),50);

    // Ajout du listener via la fonction
    canvas.addEventListener('click', explorationClickHandler);
  }


  // Suppression du listener
  ExplorationMenu.prototype.removeClickListener = function() {
    canvas.removeEventListener('click', explorationClickHandler);
  }


  // Fonction d'affichage des niveaux dans le menu exploration
  ExplorationMenu.prototype.displayLevels = function() {

    // Pour chaque niveau du monde actuel on dessine son cercle correspondant
    this.worlds[this.currentWorld].levels.forEach(level => {
      context.beginPath();
      context.arc(level.x, level.y, level.radius, 0, 2 * Math.PI, false);

      // Affichage spécifique pour les niveaux encore bloqués
      if(level.id != 1 && level.state == 'locked') {
        context.fillStyle = 'grey';
        context.fill();

        var lockIcon = new Image();
        lockIcon.onload = function() {
            context.drawImage(lockIcon, level.x-15, level.y-20, 30, 35); // Affichage svg d'un cadenas
         }
         lockIcon.src = 'assets/imgs/states/lock.svg';

        // Si le niveau n'est pas optionnel
      } else if(level.isOptionnal == false) {
        context.fillStyle = level.color;
        context.fill();
        context.fillStyle = "black";
        context.font = "25px Arial";
        context.fillText(""+level.id,level.x-9,level.y+6);

      } else {
        context.fillStyle = level.color;
        context.fill();
        context.fillStyle = "black";
        context.font = "25px Arial";
        context.fillText("S",level.x-9,level.y+6);
      }

      // Si il s'agit d'un niveau spécial
      if(level.type == 'special') {
        var meteorIcon = new Image();
        meteorIcon.onload = function() {
            context.drawImage(meteorIcon, level.x-10, level.y-70, 30, 30); // Affichage svg d'un meteor (niveau special)
         }
         meteorIcon.src = 'assets/imgs/meteor.svg';
      }

      // Si c'est le niveau du boss
      if(level.type == 'boss') {
        var skullIcon = new Image();
        skullIcon.onload = function() {
            context.drawImage(skullIcon, level.x-15, level.y-80, 30, 40); // Affichage svg d'un meteor (niveau special)
         }
         skullIcon.src = 'assets/imgs/skull.svg';
      }

      // Dessin des liens entre niveaux pour la version dev
      if(level.id != 1 && level.isOptionnal == false) {
        context.beginPath();
        context.moveTo(this.worlds[0].levels[level.id-2].x, this.worlds[0].levels[level.id-2].y);
        context.lineTo(level.x, level.y);
        context.stroke();

      } else if(level.isOptionnal == true) {
        context.beginPath();
        context.setLineDash([5, 15]);
        context.moveTo(this.worlds[0].levels[level.previousLevel-1].x, this.worlds[0].levels[level.previousLevel-1].y);
        context.lineTo(level.x, level.y);
        context.stroke();

        context.beginPath();
        context.moveTo(level.x, level.y);
        context.lineTo(this.worlds[0].levels[level.nextLevel-1].x, this.worlds[0].levels[level.nextLevel-1].y);
        context.stroke();
        context.setLineDash([]);
      }
    });

  }




  // Fonction gérant l'affichage des variables du joueur en haut du menu d'exploration
  ExplorationMenu.prototype.displayPlayerStats = function() {

     // Affichage barre d'experience
     var experienceBarImage = new Image();
     experienceBarImage.onload = function() {
         context.drawImage(experienceBarImage, experienceBarCoords.x, experienceBarCoords.y, controller.player.experience * 1.35, experienceBarCoords.height);

         // Affichage du niveau du joueur
         context.fillStyle = "white";
         context.font = "800 20px Apple SD Gothic Neo";
         context.fillText(controller.player.level, experienceBarCoords.x + 15, experienceBarCoords.y + 22);
      }
      experienceBarImage.src = 'assets/imgs/objects/ExperienceBar.png';


    // Affichage nombre de pièces de vaisseau
    context.fillStyle = "#576bc1";
    context.font = "800 25px Apple SD Gothic Neo";
    context.fillText(this.worlds[this.currentWorld].shipParts+" / 4", shipPartsLabelCoords.x, shipPartsLabelCoords.y);


    // Affichage argent du joueur
    context.fillStyle = "#576bc1";
    context.font = "800 25px Apple SD Gothic Neo";
    context.fillText(this.player.money, moneyLabelCoords.x, moneyLabelCoords.y);

  }




  // Affichage miniature de l'avatar
  ExplorationMenu.prototype.displayAvatar = function() {

    var imagesCount = 1; // Nombre d'images à charger
    var imagesLoaded = 0; // Images chargées

    // Stockage avatar
    var avatarImage = new Image();
    var avatar = this.player.defaultAvatar;
    avatarImage.src = 'assets/imgs/avatar/avatar_'+avatar+'.png';


    // Stockage des images et leur path si objet selectionné :
    // Tete
    var headImage = new Image();
    var head = this.player.equipedObjects.head;
    if(head != null) {
      imagesCount ++;
      headImage.src = 'assets/imgs/avatar/'+head+'.png';
    }

    // Corps
    var bodyImage = new Image();
    var body = this.player.equipedObjects.body;
    if(body != null) {
      imagesCount ++;
      bodyImage.src = 'assets/imgs/avatar/'+body+'.png';
    }


     // Listes des images et des parties de l'avatar
     var images = [avatarImage, headImage, bodyImage];
     var parts = [head, body];

     // Incrémentation du nb d'images chargées et affichage quand complet
     for(var i=0; i<images.length; i++){
       images[i].onload = function(){
         imagesLoaded++;
         if(imagesLoaded == imagesCount){
           context.drawImage(images[0], 20, 30, 200, 200, avatarIconCoords.x, avatarIconCoords.y, avatarIconCoords.width, avatarIconCoords.height);
           for(var j=0; j<parts.length; j++){
             context.drawImage(images[j+1], 20, 30, 200, 200, avatarIconCoords.x, avatarIconCoords.y, avatarIconCoords.width, avatarIconCoords.height);
           }
         }
       }
     }
  }


  // Affichage du background
  ExplorationMenu.prototype.displayMenuBackground = function() {

    // Stockage des images à charger
    var background = new Image();
    let id = this.currentWorld+1;
    background.src = 'assets/imgs/worlds/World_'+id+'.png';

    var interface = new Image();
    interface.src = 'assets/imgs/MainInterface.png';

    var title = new Image();
    title.src = 'assets/imgs/text/titles/Title_'+id+'.png';

    var locked = new Image();
    locked.src = 'assets/imgs/states/Locked.png';

    // Listes des images
    var images = [background, interface, title, locked];

    // Nombre d'images à charger
    var imagesCount = images.length;

    // Images chargées
    var imagesLoaded = 0;

    // Incrémentation du nb d'images chargées et affichage quand complet
    for(var i=0; i<images.length; i++){
      images[i].onload = function(){
        imagesLoaded++;
        if(imagesLoaded == imagesCount){
          for(var j=0; j<images.length-1; j++){
            context.drawImage(images[j], 0, 0, 1400, 700);
          }

          // Affichage spécifique pour les niveaux encore bloqués
          controller.explorationMenu.worlds[controller.explorationMenu.currentWorld].levels.forEach(level => {
            if(level.id != 1 && level.state == 'locked') {
              context.drawImage(locked, level.x-33, level.y-28, 70, 52);
            }
          });

        }
      }
    }

  }



  // Fonction d'affichage des boutons
  ExplorationMenu.prototype.displayMenuButtons = function () {

    // Affichage iconne flèche droite de changement de monde
    context.beginPath();
    context.arc(rightArrowIconCoords.x, rightArrowIconCoords.y, rightArrowIconCoords.radius, 0, 2 * Math.PI, false);
    context.fillStyle = 'grey';
    context.fill();


    // Affichage iconne flèche gauche de changement de monde
    context.beginPath();
    context.arc(leftArrowIconCoords.x, leftArrowIconCoords.y, leftArrowIconCoords.radius, 0, 2 * Math.PI, false);
    context.fillStyle = 'grey';
    context.fill();


    // Affichage iconne des paramètres
    context.beginPath();
    context.arc(optionsIconCoords.x, optionsIconCoords.y, optionsIconCoords.radius, 0, 2 * Math.PI, false);
    context.fillStyle = 'grey';
    context.fill();



    // Affichage iconne d'arrêt
    context.beginPath();
    context.arc(offIconCoords.x, offIconCoords.y, offIconCoords.radius, 0, 2 * Math.PI, false);
    context.fillStyle = 'grey';
    context.fill();



     // Affichage iconne boutique
     context.beginPath();
     context.arc(shopIconCoords.x, shopIconCoords.y, shopIconCoords.radius, 0, 2 * Math.PI, false);
     context.fillStyle = 'grey';
     context.fill();

  }


}
