/*
  Classe correspondant à un monde explorable du jeu qui est associé à un thème mathématique.

  Elle contient :
  -  Une liste de niveaux (objets) le composant
  -  L'état du vaisseau (pièces obtenues) permettant de passer au monde suivant
  -  Un thème mathématique indiquant le type de questions
  -  Un état qui peut-être 'locked' ou 'unlocked' selon son accessibilité
*/


function ExplorationWorld(id, name, theme) {

  this.id = id; // Id ou numéro du monde
  this.name = name; // Nom du monde/planète
  this.levels = []; // Liste des niveaux qu'il contient (définie en dur dans ExplorationMenu)
  this.shipState = 'uncomplete'; // Etat du vaisseau permettant de voyager (nécessite toutes les pièces)
  this.theme = theme; // Thème mathématiques de la planète (additions, soustractions ...)
  this.state = 'unlocked'; // Accessibilité du monde (locked, unlocked)
  this.shipParts = 0; // Nombre de pièces récupérées

}
