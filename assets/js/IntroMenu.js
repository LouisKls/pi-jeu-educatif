/*
  Classe controlant le menu d'introduction du jeu.

  Elle contient :
  - Le nombres d'images / scènes de l'intro
  - La scène courante affichée (id)

  Elle gère :
  - L'affichage des boutons interactifs
  - L'affichage de chaque scène de l'intro et leur changement
  - Le skip de l'intro
*/

function IntroMenu() {

  this.imagesNb = 5; // Nombre de scènes total
  this.currentImage = 1; // Scène courante (id)


  // Coordonnées bouton suivant
  const nextButtonCoords = {
    x: canvas.width - 67,
    y: canvas.height - 67,
    radius: 40
  }


  // Coordonnées bouton passer
  const skipButtonCoords = {
    x: 40,
    y: canvas.height - 93,
    width: 160,
    height: 60
  }



  // Handler de click pour listener
  var introMenuClickHandler = function(e) {

    var it = controller.introMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // Récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };

    // Vérifie click sur bouton suivant
    if(isCircleIntersect(mouse, nextButtonCoords)) {
      it.removeListeners();
      // Si ce n'est pas la dernière scène
      if(it.currentImage < it.imagesNb) {
        it.currentImage ++;
        controller.displayIntroMenu();
      } else {
        controller.displayExplorationMenu();
      }
    }

    // Vérifie click sur bouton passer
    if(isSquareIntersect(mouse, skipButtonCoords)) {
      it.removeListeners();
      controller.displayExplorationMenu();
    }
  }



  // Handler de la touche Entree
  var introMenuEnterHandler = function(e) {

    var it = controller.introMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)

    // Si la touche enfoncée est Entrée, meme reaction que bouton valider
    if(e.keyCode == 13) {
      it.removeListeners();
      // Si ce n'est pas la dernière scène
      if(it.currentImage < it.imagesNb) {
        it.currentImage ++;
        controller.displayIntroMenu();
      } else {
        controller.displayExplorationMenu();
      }
    }
  }


  // Lancement de la scène
  IntroMenu.prototype.launchIntroMenuScene = function() {
    this.displayBackground();
    //setTimeout(this.displayButtons.bind(this), 100);

    // Ajout des listeners via la fonction
    canvas.addEventListener('click', introMenuClickHandler);
    document.addEventListener('keyup', introMenuEnterHandler, false);
  }


  // Affichage du background
  IntroMenu.prototype.displayBackground = function() {
    var background = new Image();
    background.onload = function() {
        context.drawImage(background, 0, 0, 1400, 700);
     }
     background.src = 'assets/imgs/menus/IntroMenu_'+this.currentImage+'.png';
  }


  // Affichage des boutons
  IntroMenu.prototype.displayButtons = function() {

    // Affichage bouton suivant
    context.fillStyle = 'grey';
    context.beginPath();
    context.arc(nextButtonCoords.x, nextButtonCoords.y, nextButtonCoords.radius, 0, 2 * Math.PI, false);
    context.fill();

   // Bouton passer
   context.fillRect(skipButtonCoords.x, skipButtonCoords.y, skipButtonCoords.width, skipButtonCoords.height);
  }



  // Fermeture de la fenêtre
  IntroMenu.prototype.closeIntroMenuScene = function() {
    this.removeClickListener();
    controller.displayExplorationMenu();
  }



  // Suppression des listeners sur le menu intro
  IntroMenu.prototype.removeListeners = function() {
    canvas.removeEventListener('click', introMenuClickHandler);
    document.removeEventListener('keyup', introMenuEnterHandler);
  }
}
