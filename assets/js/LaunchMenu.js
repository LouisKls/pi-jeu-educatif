/*
  Classe controlant le menu de lancement du jeu.

  Elle gère :
  - L'affichage du menu d'accueil du jeu
  - Le lancement du jeu selon la présence d'une sauvegarde ou non via le controleur
*/

function LaunchMenu() {

  // Coordonnées bouton jouer
  const playButtonCoords = {
    x: (canvas.width/2) - 165,
    y: (canvas.height/2) + 180,
    width: 340,
    height: 120
  }


  // Fonction listener de click sur le canvas (pas de lambda expression pour pouvoir la remove apres)
  var launchClickHandler =  function(e) {

    var it = controller.launchMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };

    // Vérifie interactions avec bouton jouer
    if (isSquareIntersect(mouse, playButtonCoords)) {
      it.removeClickListener();
      controller.displayLoadingMenu();
    }
  }


  // Fonction d'affichage de la scène pour le menu de lancement
  LaunchMenu.prototype.launchLaunchMenuScene = function() {
    this.displayMenuBackground();
    //setTimeout(this.displayMenuButton.bind(this),1000);

    // Ajout du listener via la fonction
    canvas.addEventListener('click', launchClickHandler);
  }


  // Suppression du listener
  LaunchMenu.prototype.removeClickListener = function() {
    canvas.removeEventListener('click', launchClickHandler);
  }


  // Affichage du background
  LaunchMenu.prototype.displayMenuBackground = function() {
    var background = new Image();
    background.onload = function() {
        context.drawImage(background, 0, 0, 1400, 700);
     }
     background.src = 'assets/imgs/menus/LaunchMenu.png';
  }


  // Affichage du bouton
  LaunchMenu.prototype.displayMenuButton = function() {
    // Affichage bouton jouer
    context.beginPath();
    context.rect(playButtonCoords.x, playButtonCoords.y, playButtonCoords.width, playButtonCoords.height);
    context.stroke();

    context.fillStyle = "black";
    context.font = "40px Arial";
    context.fillText("Jouer", playButtonCoords.x+20, playButtonCoords.y+40);
  }
}
