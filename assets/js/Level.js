/*
  Classe correspondant à un niveau (questions).

  Elle contient :
  - Le monde (objet) auquel elle est associée
  - Le niveau d'exploration (objet) auquel elle est associée
  - Le compteur de questions
  - Le nombre de réponses justes pour le niveau courant
  - Le nombre de réponses fausses pour le niveau courant
  - La question générée actuelle
  - La réponse a la question actuelle
  - Le problème pour les niveaux dédiés
  - Le timer (questions spéciales seulement)
  - Le nombre de vies du joueur pour le niveau courant (boss seulement)
  - Le nombre de vies du boss pour le niveau courant (boss seulement)

  Elle gère :
  - L'affichage des boutons interactifs
  - L'affichage de l'entete selon le type du niveau
  - Le lancement et la reprise du niveau
  - L'affichage et la génération de la zone de réponse
  - La génération aléatoire d'une question selon le thème et la difficulté
  - L'affichage de la question
  - La vérification de la réponse apportée par le joueur
  - Le timer pour les niveaux spéciaux
  - Les vies du joueur et du boss pour les niveaux boss
  - Le listener de click et de la touche 'Entree'
*/

function Level(world, level) {

  this.world = world; // Monde associé
  this.explorationLevel = level; // Niveau d'exploration associé
  this.questionsCount = 1; // Compteur du nombre de question
  this.wins = 0; // Réponses justes
  this.fails = 0; // Réponses fausses
  this.generatedQuestion = null; // Question générée
  this.questionAnswer = null; // Réponse valide à la question
  this.questionDisplay = null; // Affichage de la question générée
  this.answerDisplay = null; // Affichage de la correction
  this.problem = null; // Problème associé (objet)
  this.timer = null; // Timer questions spéciales
  this.time = 10; // Temps aloué au timer
  this.playerLives = 10; // Vies restantes du joueur
  this.bossLives = 10; // Vies restantes du boss

  this.possibleGoodMessages = ["Super","Genial","Parfait","Bien joue"]; // Messages de réussite possibles
  this.possibleWrongMessages = ["Oups","Dommage","Rate"]; // Message d'erreur possibles


  // Coordonnées iconne pause
  const pauseIconCoords = {
    x: canvas.width - 106,
    y: 81,
    radius: 50
  }

  // Coordonnées bouton de validation
  const answerButtonCoords = {
    x: (canvas.width/2) - 103,
    y: (canvas.height/2) + 240,
    width: 210,
    height: 75
  }

  // Coordonnées iconne avatar
  const avatarIconCoords = {
    x: 238,
    y: 105,
    width: 140,
    height: 140
  }

  // Coordonnées iconne boss
  const bossIconCoords = {
    x: 1038,
    y: 105,
    width: 140,
    height: 140
  }

  // Coordonnées iconne vie
  const lifeIconCoords = {
    x: 350,
    y: 60,
    width: 50,
    height: 50
  }

  // Fonction listener de click sur le canvas (pas de lambda expression pour pouvoir la remove apres)
  var questionClickHandler =  function(e) {

    var it = controller.level; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };


    // Vérifie interactions avec iconne pause
    if (isCircleIntersect(mouse, pauseIconCoords)) {
      controller.displayPauseMenu(it);
    }

    // Vérifie interactions avec bouton validation
    if (isSquareIntersect(mouse, answerButtonCoords)) {
      if(it.explorationLevel.type == 'normal') {
        it.checkAnswer();
      } else if(it.explorationLevel.type == 'special'){
        it.checkEstimationAnswer();
      } else {
        it.checkBossAnswer();
      }
    }
  }


  // Handler de la touche Entree
  var questionEnterHandler = function(e) {

    var it = controller.level; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)

    // Si la touche enfoncée est Entrée, meme reaction que bouton valider
    if(e.keyCode == 13) {
      if(it.explorationLevel.type == 'normal') {
        it.checkAnswer();
      } else if(it.explorationLevel.type == 'special'){
        it.checkEstimationAnswer();
      } else {
        it.checkBossAnswer();
      }
    }
  }


  // Fonction d'affichage et de lancement du niveau
  Level.prototype.launchLevelScene = function() {

    this.displayMenuBackground();
    setTimeout(this.displayHeader.bind(this), 100);
    setTimeout(this.displayAnswerArea.bind(this), 100);

    // Génération de question selon type de niveau
    if(this.explorationLevel.type == 'normal' || this.explorationLevel.type == 'boss') {
      // Question générée différemment selon le thème
      if(this.world.theme == 'Additions'){
        this.generateAdditionQuestion();
        setTimeout(this.displayQuestion.bind(this), 100);
      } else if(this.world.theme == 'Soustractions') {
        this.generateSubtractionQuestion();
        setTimeout(this.displayQuestion.bind(this), 100)
      } else if(this.world.theme == 'Complément à 10') {
        this.generateComplementToTenQuestion();
        setTimeout(this.displayQuestion.bind(this), 100)
      } else if(this.world.theme == 'Doubles') {
        this.generateDoubleQuestion();
        setTimeout(this.displayQuestion.bind(this), 100)
      } else if(this.world.theme == 'Additions à trou') {
        this.generateHoleAdditionQuestion();
        setTimeout(this.displayQuestion.bind(this), 100)
      } else if(this.world.theme == 'Multiplications (tables 3 à 5)') {
        this.generateFirstMultiplicationQuestion();
        setTimeout(this.displayQuestion.bind(this), 100)
      } else if(this.world.theme == 'Multiplications (tables 6 à 9)') {
        this.generateSecondMultiplicationQuestion();
        setTimeout(this.displayQuestion.bind(this), 100)
      } else if(this.world.theme == 'Soustractions à trou') {
        this.generateHoleSubstractionQuestion();
        setTimeout(this.displayQuestion.bind(this), 100)
      } else if(this.world.theme == 'Problèmes simples') {
        this.generateSimpleProblemQuestion();
        setTimeout(this.problem.displayProblem.bind(this.problem), 100);
      } else if(this.world.theme == 'Problèmes complexes') {
        this.generateComplexProblemQuestion();
        setTimeout(this.problem.displayProblem.bind(this.problem), 100);
      }


      // Sinon niveau spécial
    } else {
      this.generateEstimationQuestion();
      setTimeout(this.displayEstimationQuestion.bind(this), 100);
    }

    // Ajout des listeners via la fonction
    canvas.addEventListener('click', questionClickHandler);
    document.addEventListener('keyup', questionEnterHandler, false);
  }



  // Reprise du niveau
  Level.prototype.resumeLevelScene = function(question, answer, answerDisplay, count) {
      this.displayMenuBackground();

      // Récupération progression
      this.questionsCount = count;
      setTimeout(this.displayHeader.bind(this), 100)
      setTimeout(this.displayAnswerArea.bind(this), 100)


      // Génération de question selon type de niveau
      this.questionDisplay = question;
      this.questionAnswer = answer;
      this.answerDisplay = answerDisplay;
      setTimeout(this.displayQuestion.bind(this), 100)


      // Ajout des listeners via la fonction
      canvas.addEventListener('click', questionClickHandler);
      document.addEventListener('keyup', questionEnterHandler, false);
  }



  // Reprise du niveau
  Level.prototype.resumeProblemLevelScene = function(problem, answer, count) {
      this.displayMenuBackground();

      // Récupération progression
      this.questionsCount = count;
      this.problem = problem;
      setTimeout(this.displayHeader.bind(this), 100)
      setTimeout(this.displayAnswerArea.bind(this), 100)


      // Génération de question selon type de niveau
      this.questionAnswer = answer;

      // Affichage du problème
      context.fillStyle = "white";
      context.font = "800 40px Apple SD Gothic Neo";
      setTimeout(context.fillText.bind(context), 100, this.problem.display[0], this.problem.displayCoords.x + 60, this.problem.displayCoords.y);
      setTimeout(context.fillText.bind(context), 100, this.problem.display[1], this.problem.displayCoords.x + 150, this.problem.displayCoords.y + 80);
      if(this.problem.type == 'complex') {
        setTimeout(context.fillText.bind(context), 100, this.problem.display[2], this.problem.displayCoords.x + 10, this.problem.displayCoords.y + 160);
      } else {
        setTimeout(context.fillText.bind(context), 100, this.problem.display[2], this.problem.displayCoords.x - 100, this.problem.displayCoords.y+ 160);
      }



      // Ajout des listeners via la fonction
      canvas.addEventListener('click', questionClickHandler);
      document.addEventListener('keyup', questionEnterHandler, false);
  }



  // Reprise du niveau
  Level.prototype.resumeSpecialLevelScene = function(question, answer, answerDisplay, count, time) {
      context.clearRect(0, 0, 1400, 700);
      this.displayMenuBackground();

      // Récupération progression
      this.questionsCount = count;
      this.time = time;

      // Affichage
      setTimeout(this.displayEstimationQuestion.bind(this), 100);
      setTimeout(this.displayHeader.bind(this), 100);
      this.displayAnswerArea();


      // Génération de question selon type de niveau
      this.questionAnswer = answer;
      this.answerDisplay = answerDisplay;


      // Ajout des listeners via la fonction
      canvas.addEventListener('click', questionClickHandler);
      document.addEventListener('keyup', questionEnterHandler, false);
  }



  // Reprise du niveau
  Level.prototype.resumeBossLevelScene = function(question, answer, answerDisplay, playerLives, bossLives) {
      context.clearRect(0, 0, 1400, 700);
      this.displayMenuBackground();

      // Récupération progression
      this.playerLives = playerLives;
      this.bossLives = bossLives;
      setTimeout(this.displayHeader.bind(this), 100);
      this.displayAnswerArea();


      this.questionDisplay = question;
      this.questionAnswer = answer;
      this.answerDisplay = answerDisplay;
      setTimeout(this.displayQuestion.bind(this), 100);


      // Ajout des listeners via la fonction
      canvas.addEventListener('click', questionClickHandler);
      document.addEventListener('keyup', questionEnterHandler, false);
  }


  // Affichage du background
  Level.prototype.displayMenuBackground = function() {
    var background = new Image();

    if(this.explorationLevel.type != 'boss') {
      background.src = 'assets/imgs/menus/Level.png';
    } else {
      background.src = 'assets/imgs/menus/BossLevel.png';
    }

    background.onload = function() {
        context.drawImage(background, 0, 0, 1400, 700);
     }
  }


  // Affichage du header selon type de niveau
  Level.prototype.displayHeader = function() {

    if(this.explorationLevel.type != 'boss') {
      // Affichage nombre de questions
      context.fillStyle = "white";
      context.font = "800 40px Apple SD Gothic Neo";
      context.fillText("Question "+this.questionsCount+" sur 10", (canvas.width/2)-155, 80);
    }

    // // Affichage iconne pause
    // context.beginPath();
    // context.arc(pauseIconCoords.x, pauseIconCoords.y, pauseIconCoords.radius, 0, 2 * Math.PI, false);
    // context.fillStyle = 'grey';
    // context.fill();


     // Vérifie type du niveau pour le header
    if(this.explorationLevel.type == 'normal') {

    // Header niveaux spéciaux
    } else if(this.explorationLevel.type == 'special') {
      this.displaySpecialHeader();

    // Header niveau boss
    } else if(this.explorationLevel.type == 'boss') {
      this.displayBossHeader();
    }
  }



  // Affichage en-tête niveau spéciaux
  Level.prototype.displaySpecialHeader = function() {
    // Affichage iconne timer
    var timerIcon = new Image();
    timerIcon.onload = function() {
      context.drawImage(timerIcon, 35, 35, 45, 50); // Affichage svg iconne timer
    }
    timerIcon.src = 'assets/imgs/timer.svg';

    // Lancement du timer
    this.timer = setInterval(function() { // Interval toutes les 1000 millisecondes (1sec)
      // Affichage timer
      controller.level.displayTimerZone();

      controller.level.time --;

      if(controller.level.time < 0) {
        clearInterval(controller.level.timer);
        controller.level.time = 10
        //alert('echec');
        controller.level.fails ++;
        // Si ce n'est pas la dernière question : suivante
        if(controller.level.questionsCount != '10') {
          controller.level.nextQuestion();
        } else {
          controller.launchLevelEnd(this);
        }
      }
    },1000);
  }

  // Affichage de la zone du timer
  Level.prototype.displayTimerZone = function() {
    var timerZone = new Image();
    timerZone.src = 'assets/imgs/objects/TimerZone.png';

    context.fillStyle = "white";
    context.font = "800 40px Apple SD Gothic Neo";

    timerZone.onload = function() {
      context.clearRect(90, 20, 80, 80); // Reset zone timer
      context.drawImage(timerZone, 90, 20, 80, 80);
      context.fillText(controller.level.time, 100, 80);
    }

  }



  // Affichage en-tête niveaux boss
  Level.prototype.displayBossHeader = function() {

    // Affichage nom
    context.fillStyle = "white";
    context.font = "800 35px Apple SD Gothic Neo";
    context.fillText("Toi", avatarIconCoords.x+35, avatarIconCoords.y-30);


    var imagesCount = 1; // Nombre d'images à charger
    var imagesLoaded = 0; // Images chargées

    // Stockage avatar
    var avatarImage = new Image();
    var avatar = controller.player.defaultAvatar;
    avatarImage.src = 'assets/imgs/avatar/avatar_'+avatar+'.png';


    // Stockage des images et leur path si objet selectionné :
    // Tete
    var headImage = new Image();
    var head = controller.player.equipedObjects.head;
    if(head != null) {
      imagesCount ++;
      headImage.src = 'assets/imgs/avatar/'+head+'.png';
    }

    // Corps
    var bodyImage = new Image();
    var body = controller.player.equipedObjects.body;
    if(body != null) {
      imagesCount ++;
      bodyImage.src = 'assets/imgs/avatar/'+body+'.png';
    }


     // Listes des images et des parties de l'avatar
     var images = [avatarImage, headImage, bodyImage];
     var parts = [head, body];

     // Incrémentation du nb d'images chargées et affichage quand complet
     for(var i=0; i<images.length; i++){
       images[i].onload = function(){
         imagesLoaded++;
         if(imagesLoaded == imagesCount){
           context.drawImage(images[0], 20, 30, 200, 200, avatarIconCoords.x, avatarIconCoords.y, avatarIconCoords.width, avatarIconCoords.height);
           for(var j=0; j<parts.length; j++){
             context.drawImage(images[j+1], 20, 30, 200, 200, avatarIconCoords.x, avatarIconCoords.y, avatarIconCoords.width, avatarIconCoords.height);
           }
         }
       }
     }

     // Affichage barre de vie joueur
     context.fillStyle = "black";

     context.beginPath();
     context.rect(avatarIconCoords.x, avatarIconCoords.y+170, avatarIconCoords.width, 30);
     context.stroke();

     // Rectangle correspondant a la progression de la barre selon vie du joueur
     context.fillRect(avatarIconCoords.x, avatarIconCoords.y+170, (avatarIconCoords.width/10)*this.playerLives, 30);

     // Iconne coeur barre de vie
     var heartIcon1 = new Image();
     heartIcon1.onload = function() {
         context.drawImage(heartIcon1, avatarIconCoords.x-50, avatarIconCoords.y+165, 40, 40);
      }
      heartIcon1.src = 'assets/imgs/life.svg';


     // Affichage nom
     context.fillStyle = "white";
     context.font = "800 35px Apple SD Gothic Neo";
     context.fillText("Boss", bossIconCoords.x+25, bossIconCoords.y-30);


     // Affichage de l'iconne du boss
     var bossIcon = new Image();
     bossIcon.onload = function() {
         context.drawImage(bossIcon, bossIconCoords.x-5, bossIconCoords.y-2, bossIconCoords.width+10, bossIconCoords.height+5); // Affichage svg d'un cadenas
      }
      // bossIcon.src = "assets/imgs/boss/boss_"+this.world.id+"_"+this.bossLives+".png";
      bossIcon.src = "assets/imgs/boss/boss_0_"+this.bossLives+".png";

      // Affichage barre de vie boss
      context.fillStyle = "black";

      context.beginPath();
      context.rect(bossIconCoords.x, bossIconCoords.y+170, bossIconCoords.width, 30);
      context.stroke();

      // Rectangle correspondant a la progression de la barre selon vie du boss
      context.fillRect(bossIconCoords.x, bossIconCoords.y+170, (bossIconCoords.width/10)*this.bossLives, 30);

      // Iconne coeur barre de vie
      var heartIcon2 = new Image();
      heartIcon2.onload = function() {
          context.drawImage(heartIcon2, bossIconCoords.x-50, bossIconCoords.y+165, 40, 40);
       }
       heartIcon2.src = 'assets/imgs/life.svg';
  }



  // Affichage de la zone de réponse
  Level.prototype.displayAnswerArea = function() {

    // Création de l'element html input par dessus le canvas
    let input = document.createElement("INPUT");

    if(this.world.id != 8) {
      input.setAttribute("type", "number");
    } else {
      input.setAttribute("type", "text");
    }

    input.id = 'inputAnswer';
    input.style.fontSize = (50/scaleYCoeff)+'px';
    input.style.outline = 'none';
    input.style.border = '1px solid white';
    input.style.textAlign = 'center';
    input.style.position = 'absolute';
    input.style.width = '12vw';

    if(scaleXCoeff == 1400/640) {
      input.style.height = '50px';
      input.style.marginTop = '271px';
    } else if(scaleXCoeff == 1400/320) {
      input.style.height = '25px';
      input.style.marginTop = '139px';
    } else {
      input.style.height = '90px';
      input.style.marginTop = '460px';
    }

    // On l'associe à la div html englobant le canvas
    let div = document.getElementById('gameDiv');
		div.appendChild(input);

    // On donne le focus à l'input
    input.focus();

    // // Affichage bouton de réponse
    // context.beginPath();
    // context.rect(answerButtonCoords.x, answerButtonCoords.y, answerButtonCoords.width, answerButtonCoords.height);
    // context.stroke();

  }


  // Affichage de la question
  Level.prototype.displayQuestion = function() {
    context.fillStyle = "white";
    context.font = "800 75px Apple SD Gothic Neo";
    context.fillText(""+this.questionDisplay, (canvas.width/2)-140, 300);
  }



  // Fonction générant une question d'addition aléatoirement en fonction de la difficulté
  Level.prototype.generateAdditionQuestion = function() {
    let operator = themeToOperator(this.world.theme); // Opérateur relatif au monde
    let firstNumber = null;
    let secondNumber = null;

    // Adaptation complexité selon difficulté
    if(controller.player.difficulty == 'easy') {
      firstNumber = Math.floor(Math.random() * 9) + 1; // Premier membre entre 1 et 9
      secondNumber = Math.floor(Math.random() * 9) + 1; // Second membre entre 1 et 9

    } else if(controller.player.difficulty == 'medium') {
      firstNumber = Math.floor(Math.random() * 40) + 10; // Entre 10 et 49
      secondNumber = Math.floor(Math.random() * 40) + 10;

    } else if(controller.player.difficulty == 'hard') {
      firstNumber = Math.floor(Math.random() * 50) + 50; // Entre 50 et 99
      secondNumber = Math.floor(Math.random() * 50) + 50;
    }

    this.generatedQuestion = ""+firstNumber+" "+operator+" "+secondNumber;
    this.questionAnswer = eval(this.generatedQuestion); // On associe la question a sa réponse calculée
    this.questionDisplay = this.generatedQuestion+" = ?"; // Affichage question
    this.answerDisplay = this.generatedQuestion+" = "+this.questionAnswer; // Affichage réponse
  }



  // Fonction générant une question de soustraction aléatoirement en fonction de la difficulté
  Level.prototype.generateSubtractionQuestion = function() {
    let operator = themeToOperator(this.world.theme); // Opérateur relatif au monde
    let firstNumber = null;
    let secondNumber = null;

    // Adaptation complexité selon difficulté
    if(controller.player.difficulty == 'easy') {
      firstNumber = Math.floor(Math.random() * 9) + 1; // Premier membre entre 1 et 9
      secondNumber = Math.floor(Math.random() * firstNumber); // Second membre entre 1 et le premier nombre (évite négatif)

    } else if(controller.player.difficulty == 'medium') {
      firstNumber = Math.floor(Math.random() * 90) + 10; // Entre 10 et 99
      secondNumber = Math.floor(Math.random() * firstNumber);

    } else if(controller.player.difficulty == 'hard') {
      firstNumber = Math.floor(Math.random() * 50) + 50; // Entre 50 et 99
      secondNumber = Math.floor(Math.random() * firstNumber);
    }

    this.generatedQuestion = ""+firstNumber+" "+operator+" "+secondNumber;
    this.questionAnswer = eval(this.generatedQuestion); // On associe la question a sa réponse calculée
    this.questionDisplay = this.generatedQuestion+" = ?"; // Affichage question
    this.answerDisplay = this.generatedQuestion+" = "+this.questionAnswer; // Affichage réponse
  }




  // Fonction générant une question spéciale aléatoirement en fonction de la difficulté
  Level.prototype.generateEstimationQuestion = function() {
    // Nombre de points affichés exact
    let pointsNumber = null;

    // Adaptation complexité selon difficulté
    if(controller.player.difficulty == 'easy') {
      pointsNumber = Math.floor(Math.random() * 7) + 2; // Entre 2 et 8

    } else if(controller.player.difficulty == 'medium') {
      pointsNumber = Math.floor(Math.random() * 21) + 10; // Entre 10 et 30

    } else if(controller.player.difficulty == 'hard') {
      pointsNumber = Math.floor(Math.random() * 31) + 20; // Entre 20 et 50
    }

     // On associe la question a sa réponse
    this.questionAnswer = pointsNumber;
    this.answerDisplay = pointsNumber;
  }


  // Fonction affichant une question d'estimation
  Level.prototype.displayEstimationQuestion = function() {
    // Affichage aléatoire de chaque point dans la zone prévue
    for(let i = 0; i < this.questionAnswer ;i++) {
      let randomX = Math.floor(Math.random() * 1201) + 100; // Entre 100 et 1300
      let randomY = Math.floor(Math.random() * 101) + 200; // Entre 200 et 400
      context.beginPath();
      context.arc(randomX, randomY, 10, 0, 2 * Math.PI, false);
      context.fillStyle = 'white';
      context.fill();
    }
  }


  // Fonction générant une question de complément à 10 aléatoirement en fonction de la difficulté
  Level.prototype.generateComplementToTenQuestion = function() {
    let operator = themeToOperator(this.world.theme); // Opérateur relatif au monde
    let number = null;

    number = Math.floor(Math.random() * 9) + 1; // Nombre entre 1 et 9

    this.generatedQuestion = ""+number;
    this.questionAnswer = 10 - number; // On associe la question a sa réponse calculée
    this.questionDisplay = this.generatedQuestion; // Affichage question
    this.answerDisplay = this.generatedQuestion+" + "+this.questionAnswer+" = 10"; // Affichage réponse
  }



  // Fonction générant une question de doubles aléatoirement
  Level.prototype.generateDoubleQuestion = function() {
    let operator = themeToOperator(this.world.theme); // Opérateur relatif au monde
    let firstNumber = 2;
    let secondNumber = null;

    secondNumber = Math.floor(Math.random() * 9) + 1; // Second membre entre 1 et 9

    this.generatedQuestion = ""+firstNumber+" "+operator+" "+secondNumber;
    this.questionAnswer = eval(this.generatedQuestion); // On associe la question a sa réponse calculée
    this.questionDisplay = ""+firstNumber+" x "+secondNumber+" = ?"; // Affichage question
    this.answerDisplay = ""+firstNumber+" x "+secondNumber+" = "+this.questionAnswer; // Affichage réponse
  }



  // Fonction générant une question d'addition à trou aléatoirement en fonction de la difficulté
  Level.prototype.generateHoleAdditionQuestion = function() {
    let operator = themeToOperator(this.world.theme); // Opérateur relatif au monde
    let firstNumber = null;
    let result = null;

    // Adaptation complexité selon difficulté
    if(controller.player.difficulty == 'easy') {
      firstNumber = Math.floor(Math.random() * 9) + 1; // Premier membre entre 1 et 9
      result = Math.floor(Math.random() * 21-firstNumber) + firstNumber; // Résultat entre premier membre et 20

    } else if(controller.player.difficulty == 'medium') {
      firstNumber = Math.floor(Math.random() * 9) + 1; // Premier membre entre 1 et 9
      result = Math.floor(Math.random() * 51-firstNumber) + firstNumber; // Résultat entre premier membre et 50

    } else if(controller.player.difficulty == 'hard') {
      firstNumber = Math.floor(Math.random() * 9) + 1; // Premier membre entre 1 et 9
      result = Math.floor(Math.random() * 101-firstNumber) + firstNumber; // Résultat entre premier membre et 100
    }

    this.questionAnswer = result - firstNumber; // On associe la question a sa réponse calculée
    this.questionDisplay = ""+firstNumber+" "+operator+" ? = "+result; // Affichage question
    this.answerDisplay = ""+firstNumber+" "+operator+" "+this.questionAnswer+" = "+result; // Affichage question
  }



  // Fonction générant une question de multiplication des tables 3 à 5 aléatoirement
  Level.prototype.generateFirstMultiplicationQuestion = function() {
    let operator = themeToOperator(this.world.theme); // Opérateur relatif au monde
    let firstNumber = null;
    let secondNumber = null;

    firstNumber = Math.floor(Math.random() * 3) + 3; // Premier membre entre 3 et 5
    secondNumber = Math.floor(Math.random() * 9) + 1; // Second membre entre 1 et 9

    this.generatedQuestion = ""+firstNumber+" "+operator+" "+secondNumber;
    this.questionAnswer = eval(this.generatedQuestion); // On associe la question a sa réponse calculée
    this.questionDisplay = ""+firstNumber+" x "+secondNumber+" = ?"; // Affichage question
    this.answerDisplay = ""+firstNumber+" x "+secondNumber+" = "+this.questionAnswer; // Affichage réponse
  }



  // Fonction générant une question de multiplication des tables 6 à 9 aléatoirement
  Level.prototype.generateSecondMultiplicationQuestion = function() {
    let operator = themeToOperator(this.world.theme); // Opérateur relatif au monde
    let firstNumber = null;
    let secondNumber = null;

    firstNumber = Math.floor(Math.random() * 4) + 6; // Premier membre entre 6 et 9
    secondNumber = Math.floor(Math.random() * 9) + 1; // Second membre entre 1 et 9

    this.generatedQuestion = ""+firstNumber+" "+operator+" "+secondNumber;
    this.questionAnswer = eval(this.generatedQuestion); // On associe la question a sa réponse calculée
    this.questionDisplay = ""+firstNumber+" x "+secondNumber+" = ?"; // Affichage question
    this.answerDisplay = ""+firstNumber+" x "+secondNumber+" = "+this.questionAnswer; // Affichage réponse
  }



  // Fonction générant une question de soustraction à trou aléatoirement en fonction de la difficulté
  Level.prototype.generateHoleSubstractionQuestion = function() {
    let operator = themeToOperator(this.world.theme); // Opérateur relatif au monde
    let firstNumber = null;
    let result = null;

    // Adaptation complexité selon difficulté
    if(controller.player.difficulty == 'easy') {
      firstNumber = Math.floor(Math.random() * 20) + 1; // Premier membre entre 1 et 20
      result = Math.floor(Math.random() * firstNumber); // Résultat entre 0 et le premier membre

    } else if(controller.player.difficulty == 'medium') {
      firstNumber = Math.floor(Math.random() * 50) + 1; // Premier membre entre 1 et 50
      result = Math.floor(Math.random() * firstNumber); // Résultat entre 0 et le premier membre

    } else if(controller.player.difficulty == 'hard') {
      firstNumber = Math.floor(Math.random() * 100) + 1; // Premier membre entre 1 et 100
      result = Math.floor(Math.random() * firstNumber); // Résultat entre 0 et le premier membre
    }

    this.questionAnswer = firstNumber- result; // On associe la question a sa réponse calculée
    this.questionDisplay = ""+firstNumber+" "+operator+" ? = "+result; // Affichage question
    this.answerDisplay = ""+firstNumber+" "+operator+" "+this.questionAnswer+" = "+result; // Affichage question
  }


  // Fonction générant un problème simple aléatoirement en fonction de la difficulté
  Level.prototype.generateSimpleProblemQuestion = function() {
    this.problem = new Problem('simple');
    this.problem.generateProblem();

    this.questionAnswer = this.problem.answer;
  }


  // Fonction générant un problème complexe aléatoirement en fonction de la difficulté
  Level.prototype.generateComplexProblemQuestion = function() {
    this.problem = new Problem('complex');
    this.problem.generateProblem();

    this.questionAnswer = this.problem.answer;
  }



  // Fonction vérifiant la réponse du joueur et la vraie par comparaison
  Level.prototype.checkAnswer = function() {
    // On récupere l'entrée clavier dans la balise input
    let input = document.getElementById("inputAnswer");
    let playerAnswer = input.value;

    // Si le joueur n'a pas entré de réponse avant de valider
    if(playerAnswer == "" || playerAnswer == null) {
      var notif = new Notification("Tu dois entrer une reponse avant de valider", 330);
      notif.displayNotification();

      // Si le joueur n'a pas entré un nombre
    } else if(isNaN(playerAnswer) && this.world.theme != 'Problèmes simples') {
      var notif = new Notification("Ta reponse doit etre un nombre", 400);
      notif.displayNotification();

        // Si le joueur n'a pas entré un opérateur
    } else if(this.world.theme == 'Problèmes simples' && playerAnswer != '+' && playerAnswer != '-' ) {
      var notif = new Notification("Ta reponse doit etre un operateur ('+' ou '-')", 360);
      notif.displayNotification();

    // Sinon si la réponse du joueur correspond à la réponse calculée (bonne réponse)
    } else if(playerAnswer == this.questionAnswer) {
      this.displayGoodAnswer();
      this.wins ++;
      // Si ce n'est pas la dernière question: suivante
      if(this.questionsCount != '10') {
        this.removeListeners();
        setTimeout(this.nextQuestion.bind(this), 2000);
      } else {
        this.removeListeners();
        setTimeout(controller.launchLevelEnd.bind(controller), 2000, this);
      }

      // Sinon échec (mauvaise réponse)
    } else {
      this.displayWrongAnswer();
      this.fails ++;
      // Si ce n'est pas la dernière question : suivante
      if(this.questionsCount != '10') {
        this.removeListeners();
        setTimeout(this.nextQuestion.bind(this), 2000);
      } else {
        this.removeListeners();
        setTimeout(controller.launchLevelEnd.bind(controller), 2000, this);
      }
    }
  }


  // Fonction vérifiant la réponse du joueur et la vraie par comparaison
  Level.prototype.checkEstimationAnswer = function() {
    // On récupere l'entrée clavier dans la balise input
    let input = document.getElementById("inputAnswer");
    let playerAnswer = input.value;

    // Si le joueur n'a pas entré de réponse avant de valider
    if(playerAnswer == "" || playerAnswer == null) {
      alert("Tu dois entrer une réponse avant de valider !");

    // Sinon si la réponse du joueur correspond à la réponse calculée (bonne réponse)
    } else if(playerAnswer == this.questionAnswer) {
      //alert('réussite');
      this.displayGoodAnswer();
      this.wins ++;
      // Si ce n'est pas la dernière question: suivante
      if(this.questionsCount != '10') {
        clearInterval(this.timer);
        this.time = 10;
        this.removeListeners();
        setTimeout(this.nextQuestion.bind(this), 2000);
        // Sinon derniere question
      } else {
        clearInterval(this.timer);
        this.time = 10;
        this.removeListeners();
        setTimeout(controller.launchLevelEnd.bind(controller), 2100, this);
      }

      // Sinon si estimation proche selon difficultée : acceptée
    } else if((controller.player.difficulty == 'easy' && Math.abs(playerAnswer - this.questionAnswer) < 2) || (controller.player.difficulty == 'medium' && Math.abs(playerAnswer - this.questionAnswer) < 5) || (controller.player.difficulty == 'hard' && Math.abs(playerAnswer - this.questionAnswer) < 8)) {
      //alert('réussite');
      this.displayGoodAnswer();
      this.wins ++;
      // Si ce n'est pas la dernière question: suivante
      if(this.questionsCount != '10') {
        clearInterval(this.timer);
        this.time = 10;
        this.removeListeners();
        setTimeout(this.nextQuestion.bind(this), 2000);
        // Sinon derniere question
      } else {
        clearInterval(this.timer);
        this.time = 10;
        this.removeListeners();
        setTimeout(controller.launchLevelEnd.bind(controller), 2100, this);
      }

      // Sinon échec (mauvaise réponse)
    } else {
      //alert('echec');
      this.displayWrongAnswer();
      this.fails ++;
      // Si ce n'est pas la dernière question : suivante
      if(this.questionsCount != '10') {
        clearInterval(this.timer);
        this.time = 10;
        this.removeListeners();
        setTimeout(this.nextQuestion.bind(this), 2000);
      } else {
        clearInterval(this.timer);
        this.time = 10;
        this.removeListeners();
        setTimeout(controller.launchLevelEnd.bind(controller), 2100, this);
      }
    }
  }


  // Fonction vérifiant la réponse du joueur et la vraie par comparaison
  Level.prototype.checkBossAnswer = function() {
    // On récupere l'entrée clavier dans la balise input
    let input = document.getElementById("inputAnswer");
    let playerAnswer = input.value;

    // Si le joueur n'a pas entré de réponse avant de valider
    if(playerAnswer == "" || playerAnswer == null) {
      alert("Tu dois entrer une réponse avant de valider !");

      // Si le joueur n'a pas entré un nombre
    } else if(isNaN(playerAnswer)) {
        alert("Ta réponse doit être un nombre");

    // Sinon si la réponse du joueur correspond à la réponse calculée (bonne réponse)
    } else if(playerAnswer == this.questionAnswer) {
      this.displayGoodAnswer();
      this.bossLives --;
      // Si ce n'est pas la dernière question: suivante
      if(this.bossLives != 0 && this.playerLives != 0) {
        this.removeListeners();
        setTimeout(this.nextQuestion.bind(this), 2000);
      } else {
        this.removeListeners();
        setTimeout(this.displayBossHeader.bind(this), 2000);
        setTimeout(controller.launchLevelEnd.bind(controller), 2100, this);
      }

      // Sinon échec (mauvaise réponse)
    } else {
      this.displayWrongAnswer();
      this.playerLives --;

      // Si nb de vies pas au maximum
      if(this.bossLives < 10) {
        this.bossLives ++;
      }

      // Si ce n'est pas la dernière question: suivante
      if(this.bossLives != 0 && this.playerLives != 0) {
        this.removeListeners();
        setTimeout(this.nextQuestion.bind(this), 2000);
      } else {
        this.removeListeners();
        setTimeout(this.displayBossHeader.bind(this), 2000);
        setTimeout(controller.launchLevelEnd.bind(controller), 2100, this);
      }
    }
  }


  // Passe à la question suivante après un reset
  Level.prototype.nextQuestion = function() {
    // Nettoyage de la scène sur le canvas
    controller.clearScene();

    this.removeInputBox();
    this.removeListeners();

    // Augmentation du compteur et reset de la question
    this.questionsCount ++;
    this.generatedQuestion = null;
    this.questionAnswer = null;

    // On relance l'affichage
    this.launchLevelScene();
  }



  // Affichage bonne réponse
  Level.prototype.displayGoodAnswer = function() {
    // Couleur bonne réponse
    // let inputArea = document.getElementById('inputAnswer');
    // inputArea.style.border = 'green solid 5px';

    // Animation pièce
    var img = new Image();
    var canvasTmp = document.getElementById('canvasTmp');
    canvasTmp.style.visibility = 'visible';

    contextTmp.fillStyle = "white";
    contextTmp.font = "800 50px Apple SD Gothic Neo";


    img.src = 'assets/imgs/objects/coin.png';
    img.onload = function() {
      var x = (canvas.width/2) + 190, y = 40;
        var interval = setInterval(function() {

            contextTmp.clearRect(0, 0, 1400, 700);
            contextTmp.fillText("+", (canvasTmp.width/2) + 150, 75);
            contextTmp.drawImage(img, x, y, 40, 40);

            y -= 1;
            if (y < 20) {
              clearInterval(interval);
              contextTmp.clearRect(0, 0, 1400, 700);
              canvasTmp.style.visibility = 'hidden';
            }
        }, 35);

      }


    // Affichage du message
    setTimeout(this.displayGoodMessage.bind(this), 110);


    // Si pas theme problèmes
    if(this.world.id != 8 && this.world.id != 9) {

      // Reset
      context.clearRect(0, 0, 1400, 700);
      this.displayMenuBackground();

      // Affichage de la réponse
      context.fillStyle = "white";
      context.font = "800 75px Apple SD Gothic Neo";
      setTimeout(context.fillText.bind(context), 100, this.answerDisplay, (canvas.width/2)-140, 300);

    } else {
      // Reset
      context.clearRect(0, 0, 1400, 700);
      this.displayMenuBackground();

      // Affichage de la réponse
      context.fillStyle = "white";
      context.font = "800 40px Apple SD Gothic Neo";
      setTimeout(context.fillText.bind(context), 100, this.problem.display[0], this.problem.displayCoords.x + 60, this.problem.displayCoords.y);
      setTimeout(context.fillText.bind(context), 100, this.problem.display[1], this.problem.displayCoords.x + 150, this.problem.displayCoords.y + 80);
      if(this.problem.type == 'complex') {
        setTimeout(context.fillText.bind(context), 100, this.problem.display[2], this.problem.displayCoords.x + 10, this.problem.displayCoords.y + 160);
      } else {
        setTimeout(context.fillText.bind(context), 100, this.problem.display[2], this.problem.displayCoords.x - 100, this.problem.displayCoords.y+ 160);
      }
    }

    if(this.explorationLevel.type == 'boss') {
      setTimeout(this.displayBossHeader.bind(this), 100);
    }

  }


  // Affichage message bonne réponse
  Level.prototype.displayGoodMessage = function() {
    // Message aléatoire
    let i = Math.floor(Math.random() * 4);
    let message = this.possibleGoodMessages[i];

    // Affichage
    context.fillStyle = "white";
    context.font = "800 50px Apple SD Gothic Neo";
    context.fillText(message+" !", (canvas.width/2)-100, 80);
  }



  // Affichage iconne bonne réponse
  Level.prototype.displayGoodAnswerIcon = function() {
    var goodIcon = new Image();
    goodIcon.onload = function() {
      context.drawImage(goodIcon, (canvas.width/2) + 200, (canvas.height/2) + 170, 50, 50); // Affichage svg iconne check
    }
    goodIcon.src = 'assets/imgs/good.svg';
  }



  // Affichage mauvaise réponse
  Level.prototype.displayWrongAnswer = function() {
    // // Couleur bonne réponse
    // let inputArea = document.getElementById('inputAnswer');
    // inputArea.style.border = 'red solid 5px';

    // Affichage du message
    setTimeout(this.displayWrongMessage.bind(this), 110);


    if(this.world.id != 8 && this.world.id != 9) {
      // Reset
      context.clearRect(0, 0, 1400, 700);
      this.displayMenuBackground();

      // Affichage de la réponse
      context.fillStyle = "white";
      context.font = "800 75px Apple SD Gothic Neo";
      setTimeout(context.fillText.bind(context), 100, this.answerDisplay, (canvas.width/2)-140, 300);

    } else {
      // Reset
      context.clearRect(0, 0, 1400, 700);
      this.displayMenuBackground();

      // Affichage de la réponse
      context.fillStyle = "white";
      context.font = "800 40px Apple SD Gothic Neo";
      setTimeout(context.fillText.bind(context), 100, this.problem.display[0], this.problem.displayCoords.x + 60, this.problem.displayCoords.y);
      setTimeout(context.fillText.bind(context), 100, this.problem.display[1], this.problem.displayCoords.x + 150, this.problem.displayCoords.y + 80);
      if(this.problem.type == 'complex') {
        setTimeout(context.fillText.bind(context), 100, this.problem.display[2], this.problem.displayCoords.x + 10, this.problem.displayCoords.y + 160);
      } else {
        setTimeout(context.fillText.bind(context), 100, this.problem.display[2], this.problem.displayCoords.x - 100, this.problem.displayCoords.y+ 160);
      }
    }

    if(this.explorationLevel.type == 'boss') {
      setTimeout(this.displayBossHeader.bind(this), 100);
    }
  }


  // Affichage message mauvaise réponse
  Level.prototype.displayWrongMessage = function() {
    // Message aléatoire
    let i = Math.floor(Math.random() * 3);
    let message = this.possibleWrongMessages[i];

    // Affichage
    context.fillStyle = "white";
    context.font = "800 50px Apple SD Gothic Neo";
    context.fillText(message+" ...", (canvas.width/2)-90, 80);
  }


  // Affichage iconne mauvaise réponse
  Level.prototype.displayWrongAnswerIcon = function() {
    var wrongIcon = new Image();
    wrongIcon.onload = function() {
      context.drawImage(wrongIcon, (canvas.width/2) + 200, (canvas.height/2) + 170, 50, 50); // Affichage svg iconne croix
    }
    wrongIcon.src = 'assets/imgs/wrong.svg';
  }



  // Suppression de la zone d'input clavier
  Level.prototype.removeInputBox = function() {
    // Suppresion de la balise HTML d'input via le parent
    let input = document.getElementById("inputAnswer");
    let div = document.getElementById('gameDiv');
    div.removeChild(input);
  }


  // Suppression des listeners
  Level.prototype.removeListeners = function() {
    canvas.removeEventListener('click', questionClickHandler);
    document.removeEventListener('keyup', questionEnterHandler);
  }

}
