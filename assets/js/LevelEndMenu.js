/*
  Classe controllant le menu de fin de niveau.

  Elle contient :
  - Le niveau (objet) auquel elle est associée

  Elle gère :
  - L'affichage des boutons interactifs
  - L'affichage de la fenêtre modale
  - L'affichage des étoiles et des récompenses obtenues
  - Le listener
*/

function LevelEndMenu(level) {

  this.level = level; // Objet niveau

  // Coordonnées pour l'affichage de la fenêtre modale
  const menuCoords = {
    x: (canvas.width/2) - 350,
    y: 40,
    width: 700,
    height: 640
  }


  // Coordonnées bouton continuer
  const continueButtonCoords = {
    x: (canvas.width/2) - 128,
    y: canvas.height - 151,
    width: 273,
    height: 60
  }

  // Coordonnées bouton reessayer
  const tryAgainButtonCoords = {
    x: (canvas.width/2) - 90,
    y: canvas.height - 200,
    width: 200,
    height: 60
  }


  // Handler de click pour le listener
  var levelEndMenuClickHandler = function(e) {

    var it = controller.levelEndMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };


    // Vérifie click sur bouton continuer
    if(isSquareIntersect(mouse, continueButtonCoords)) {
      it.removeClickListener();
      controller.displayExplorationMenu();
    }

    // Vérifie click sur bouton reessayer
    if(isSquareIntersect(mouse, tryAgainButtonCoords)) {
      it.removeClickListener();
      controller.launchLevel(it.level.world, it.level.explorationLevel);
    }
  }



  // Fonction de lancement du menu post niveau
  LevelEndMenu.prototype.launchLevelEndScene = function() {
    this.displayMenuBackground('win');
    setTimeout(this.displayText.bind(this), 100, this.level.explorationLevel);
    setTimeout(this.displayStars.bind(this), 100, this.level.explorationLevel);
    //setTimeout(this.displayButtons.bind(this), 100);

    // Ajout du listener via la fonction
    canvas.addEventListener('click', levelEndMenuClickHandler);
  }



  // Fonction de lancement du menu post niveau si perdu
  LevelEndMenu.prototype.launchLostLevelEndScene = function() {
    this.displayMenuBackground('lose');
    //setTimeout(this.displayButtons.bind(this), 100);

    // Ajout du listener via la fonction
    canvas.addEventListener('click', levelEndMenuClickHandler);
  }


  // Affichage du background
  LevelEndMenu.prototype.displayMenuBackground = function(state) {
    // Stockage des images à charger
    var background = new Image();

    if(state == 'win') {
      background.src = 'assets/imgs/menus/LevelEndMenu.png';
    } else {
      background.src = 'assets/imgs/menus/LevelEndMenu_lose.png';
    }

    // Listes des images
    var images = [background];

    // Nombre d'images à charger
    var imagesCount = images.length;

    // Images chargées
    var imagesLoaded = 0;

    // Incrémentation du nb d'images chargées et affichage quand complet
    for(var i=0; i<images.length; i++){
      images[i].onload = function(){
        imagesLoaded++;
        if(imagesLoaded == imagesCount){
          for(var j=0; j<images.length; j++){
            context.drawImage(images[j], 0, 0, 1400, 700);
          }
        }
      }
    }

  }



  // Affichage du texte et des récompenses
  LevelEndMenu.prototype.displayText = function(level) {
    // Si niveau réussi
    if(level.stars >= 1) {
      // Argent gagné
      context.fillStyle = "white";
      context.font = "800 40px Apple SD Gothic Neo";
      context.fillText(""+level.moneyGiven*level.stars, (canvas.width/2)+50, 404);

      // Experience gagnée
      context.fillStyle = "white";
      context.font = "800 40px Apple SD Gothic Neo";
      context.fillText(""+level.experienceGiven*level.stars, (canvas.width/2)+50, 494);
    }
  }


  // Affichage des étoiles
  LevelEndMenu.prototype.displayStars = function(level) {
    // Stockage des images à charger
     var star1 = new Image();
     star1.src = 'assets/imgs/objects/Star_1.png';

     var star2 = new Image();
     star2.src = 'assets/imgs/objects/Star_2.png';

     var star3 = new Image();
     star3.src = 'assets/imgs/objects/Star_3.png';

     // Listes des images
     var images = [];
     if(level.stars == 3) {
       images.push(star1);
       images.push(star2);
       images.push(star3);
     } else if(level.stars == 2) {
       images.push(star1);
       images.push(star2);
     } else if(level.stars == 1) {
       images.push(star1);
     }

     // Nombre d'images à charger
     var imagesCount = images.length;

     // Images chargées
     var imagesLoaded = 0;

     // Incrémentation du nb d'images chargées et affichage quand complet
     for(var i=0; i<images.length; i++){
       images[i].onload = function(){
         imagesLoaded++;
         if(imagesLoaded == imagesCount){
           for(var j=0; j<images.length; j++){
             context.drawImage(images[j], 0, 0, 1400, 700);
           }
         }
       }
     }
  }


  // Affichage des boutons
  LevelEndMenu.prototype.displayButtons = function() {
     // Affichage bouton continuer
     context.beginPath();
     context.rect(continueButtonCoords.x, continueButtonCoords.y, continueButtonCoords.width, continueButtonCoords.height);
     context.stroke();
  }


  // Fermeture de la fenêtre
  LevelEndMenu.prototype.closeLevelEndMenuScene = function() {
    this.removeClickListener();
    controller.displayExplorationMenu();
  }


  // Suppression du listener de click sur le menu post-niveau
  LevelEndMenu.prototype.removeClickListener = function() {
    canvas.removeEventListener('click', levelEndMenuClickHandler);
  }
}
