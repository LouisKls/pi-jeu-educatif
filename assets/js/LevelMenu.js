/*
  Classe controlant le menu pré-niveau.

  Elle contient :
  - Le monde (objet) auquel elle est associée
  - Le niveau d'exploration (objet) auquel elle est associée

  Elle gère :
  - L'affichage des boutons interactifs
  - L'affichage de la fenêtre modale
  - L'affichage des étoiles déjà obtenues pour le niveau
  - Le thème et l'id du niveau
  - Le lancement du niveau
  - Le listener
*/

function LevelMenu(world, level) {

  this.world = world; // Monde associé
  this.explorationLevel = level; // Niveau d'exploration associé


  // Coordonnées pour l'affichage de la fenêtre modale
  const menuCoords = {
    x: (canvas.width/2) - 350,
    y: 40,
    width: 700,
    height: 600
  }

  // Coordonnées iconne de fermeture
  const crossIconCoords = {
    x: canvas.width - 106,
    y: 81,
    radius: 50
  }

  // Coordonnées bouton de lancement
  const startButtonCoords = {
    x: (canvas.width/2) - 128,
    y: canvas.height - 151,
    width: 273,
    height: 60
  }


  // Handler de click pour le listener
  var levelMenuClickHandler = function(e) {

    var it = controller.levelMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };

    // Vérifie click sur l'iconne de fermeture
    if(isCircleIntersect(mouse, crossIconCoords)) {
      it.closeLevelMenuScene();
    }

    // Vérifie click sur bouton de lancement
    if(isSquareIntersect(mouse, startButtonCoords)) {
      controller.launchLevel(it.world, it.explorationLevel);
    }
  }


  // Affichage du menu pré-niveau
  LevelMenu.prototype.launchLevelMenuScene = function() {
    this.displayMenuBackground(this.explorationLevel);
    setTimeout(this.displayStars.bind(this), 100, this.explorationLevel);
    //setTimeout(this.displayButtons.bind(this), 100);

    // Ajout du listener via la fonction
    canvas.addEventListener('click', levelMenuClickHandler);
  }



  // Affichage du background
  LevelMenu.prototype.displayMenuBackground = function(level) {
    // Stockage des images à charger
    var background = new Image();
    background.src = 'assets/imgs/menus/LevelMenu.png';

    var title = new Image();
    let levelId = level.id;
    title.src = 'assets/imgs/text/level_titles/LevelTitle_'+levelId+'.png';

    var theme = new Image();
    let worldId = controller.explorationMenu.currentWorld+1;
    theme.src = 'assets/imgs/text/themes/Theme_'+worldId+'.png';

    // Listes des images
    var images = [background, title, theme];

    // Nombre d'images à charger
    var imagesCount = images.length;

    // Images chargées
    var imagesLoaded = 0;

    // Incrémentation du nb d'images chargées et affichage quand complet
    for(var i=0; i<images.length; i++){
      images[i].onload = function(){
        imagesLoaded++;
        if(imagesLoaded == imagesCount){
          for(var j=0; j<images.length; j++){
            context.drawImage(images[j], 0, 0, 1400, 700);
          }
        }
      }
    }

  }



  // Affichage des étoiles
  LevelMenu.prototype.displayStars = function(level) {
      // Stockage des images à charger
       var star1 = new Image();
       star1.src = 'assets/imgs/objects/Star_1.png';

       var star2 = new Image();
       star2.src = 'assets/imgs/objects/Star_2.png';

       var star3 = new Image();
       star3.src = 'assets/imgs/objects/Star_3.png';

       // Listes des images
       var images = [];
       if(level.stars == 3) {
         images.push(star1);
         images.push(star2);
         images.push(star3);
       } else if(level.stars == 2) {
         images.push(star1);
         images.push(star2);
       } else if(level.stars == 1) {
         images.push(star1);
       }

       // Nombre d'images à charger
       var imagesCount = images.length;

       // Images chargées
       var imagesLoaded = 0;

       // Incrémentation du nb d'images chargées et affichage quand complet
       for(var i=0; i<images.length; i++){
         images[i].onload = function(){
           imagesLoaded++;
           if(imagesLoaded == imagesCount){
             for(var j=0; j<images.length; j++){
               context.drawImage(images[j], 0, 0, 1400, 700);
             }
           }
         }
       }
  }


  // Affichage des boutons
  LevelMenu.prototype.displayButtons = function() {
    // Affichage iconne de fermeture
    context.beginPath();
    context.arc(crossIconCoords.x, crossIconCoords.y, crossIconCoords.radius, 0, 2 * Math.PI, false);
    context.fillStyle = 'grey';
    context.fill();

     // Affichage bouton de lancement
     context.beginPath();
     context.rect(startButtonCoords.x, startButtonCoords.y, startButtonCoords.width, startButtonCoords.height);
     context.stroke();
  }


  // Fermeture de la fenêtre
  LevelMenu.prototype.closeLevelMenuScene = function() {
    this.removeClickListener();
    controller.displayExplorationMenu();
  }


  // Suppression du listener de click sur le menu pré-niveau
  LevelMenu.prototype.removeClickListener = function() {
    canvas.removeEventListener('click', levelMenuClickHandler);
  }

}
