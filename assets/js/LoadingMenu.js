function LoadingMenu() {

  // Fonction d'affichage de la scène pour le menu de chargement
  LoadingMenu.prototype.launchLoadingMenuScene = function() {
    this.displayMenuBackground();
    this.loadImages();
  }


  // Fonction d'affichage du bakcground
  LoadingMenu.prototype.displayMenuBackground = function() {
    // Background
    let loadingMenuImage = new Image();
    loadingMenuImage.src = 'assets/imgs/menus/LoadingMenu.png';
    loadingMenuImage.onload = function() {
      context.drawImage(loadingMenuImage, 0, 0, 1400, 700);

      // Dessin barre de chargement
      context.fillStyle = "white";
      context.beginPath();
      context.rect(330, 350, 740, 50);
      context.stroke();
    }
  }


  // Fonction de chargement des images
  LoadingMenu.prototype.loadImages = function() {
    var imagesCount = 0; // Nombre d'images à charger
    var imagesLoaded = 0; // Images chargées
    var images = [];

    // Stockage avatar
    for(let i=1; i<2; i++) {
      let avatarImage = new Image();
      avatarImage.src = 'assets/imgs/avatar/avatar_'+i+'.png';
      images.push(avatarImage);
      imagesCount ++;
    }

    // Stockage interface
    let interfaceImage = new Image();
    interfaceImage.src = 'assets/imgs/MainInterface.png';
    images.push(interfaceImage);
    imagesCount ++;


    // Stockage boss
    for(let i=1; i<10; i++) {
      let bossImage = new Image();
      bossImage.src = 'assets/imgs/boss/boss_0_'+i+'.png';
      images.push(bossImage);
      imagesCount ++;
    }


    // Stockage mondes
    for(let i=1; i<10; i++) {
      let worldImage = new Image();
      worldImage.src = 'assets/imgs/worlds/World_'+i+'.png';
      images.push(worldImage);
      imagesCount ++;
    }


    // Stockage titres
    for(let i=1; i<10; i++) {
      let titleImage = new Image();
      titleImage.src = 'assets/imgs/text/titles/Title_'+i+'.png';
      images.push(titleImage);
      imagesCount ++;
    }


    // Stockage titres niveaux
    for(let i=1; i<10; i++) {
      let levelTitleImage = new Image();
      levelTitleImage.src = 'assets/imgs/text/level_titles/LevelTitle_'+i+'.png';
      images.push(levelTitleImage);
      imagesCount ++;
    }


    // Stockage themes
    for(let i=1; i<10; i++) {
      let themeImage = new Image();
      themeImage.src = 'assets/imgs/text/themes/Theme_'+i+'.png';
      images.push(themeImage);
      imagesCount ++;
    }


    // Stockage objets
    let coinImage = new Image();
    coinImage.src = 'assets/imgs/objects/coin.png';
    images.push(coinImage);
    imagesCount ++;

    let experienceImage = new Image();
    experienceImage.src = 'assets/imgs/objects/ExperienceBar.png';
    images.push(experienceImage);
    imagesCount ++;

    let notificationImage = new Image();
    notificationImage.src = 'assets/imgs/objects/Notification.png';
    images.push(notificationImage);
    imagesCount ++;

    for(let i=1; i<3; i++) {
      let starImage = new Image();
      starImage.src = 'assets/imgs/objects/Star_'+i+'.png';
      images.push(starImage);
      imagesCount ++;
    }

    let timerImage = new Image();
    timerImage.src = 'assets/imgs/objects/TimerZone.png';
    images.push(timerImage);
    imagesCount ++;


    //Stockage Menus
    let launchMenuImage = new Image();
    launchMenuImage.src = 'assets/imgs/menus/LaunchMenu.png';
    images.push(launchMenuImage);
    imagesCount ++;

    let avatarMenuImage = new Image();
    avatarMenuImage.src = 'assets/imgs/menus/AvatarMenu.png';
    images.push(avatarMenuImage);
    imagesCount ++;

    let avatarMenuSelectionImage = new Image();
    avatarMenuSelectionImage.src = 'assets/imgs/menus/AvatarSelectionMenu.png';
    images.push(avatarMenuSelectionImage);
    imagesCount ++;

    let bossLevelImage = new Image();
    bossLevelImage.src = 'assets/imgs/menus/BossLevel.png';
    images.push(bossLevelImage);
    imagesCount ++;

    let levelImage = new Image();
    levelImage.src = 'assets/imgs/menus/Level.png';
    images.push(levelImage);
    imagesCount ++;

    let levelEndMenuImage = new Image();
    levelEndMenuImage.src = 'assets/imgs/menus/LevelEndMenu.png';
    images.push(levelEndMenuImage);
    imagesCount ++;

    let levelEndMenuLoseImage = new Image();
    levelEndMenuLoseImage.src = 'assets/imgs/menus/LevelEndMenu_lose.png';
    images.push(levelEndMenuLoseImage);
    imagesCount ++;

    let levelMenuImage = new Image();
    levelMenuImage.src = 'assets/imgs/menus/LevelMenu.png';
    images.push(levelMenuImage);
    imagesCount ++;

    let modeMenuImage = new Image();
    modeMenuImage.src = 'assets/imgs/menus/ModeMenu.png';
    images.push(modeMenuImage);
    imagesCount ++;

    let optionsMenuImage = new Image();
    optionsMenuImage.src = 'assets/imgs/menus/OptionsMenu.png';
    images.push(optionsMenuImage);
    imagesCount ++;

    let pauseMenuImage = new Image();
    pauseMenuImage.src = 'assets/imgs/menus/PauseMenu.png';
    images.push(pauseMenuImage);
    imagesCount ++;

    let shopMenuImage = new Image();
    shopMenuImage.src = 'assets/imgs/menus/ShopMenu.png';
    images.push(shopMenuImage);
    imagesCount ++;

    let usernameMenuImage = new Image();
    usernameMenuImage.src = 'assets/imgs/menus/UsernameMenu.png';
    images.push(usernameMenuImage);
    imagesCount ++;

    for(let i=1; i<5; i++) {
      let introMenuImage = new Image();
      introMenuImage.src = 'assets/imgs/menus/IntroMenu_'+i+'.png';
      images.push(introMenuImage);
      imagesCount ++;
    }


    // Stockage états
    let lockedImage = new Image();
    lockedImage.src = 'assets/imgs/states/Locked.png';
    images.push(lockedImage);
    imagesCount ++;

    let selectedEasyImage = new Image();
    selectedEasyImage.src = 'assets/imgs/states/Selected_easy.png';
    images.push(selectedEasyImage);
    imagesCount ++;

    let selectedMediumImage = new Image();
    selectedMediumImage.src = 'assets/imgs/states/Selected_medium.png';
    images.push(selectedMediumImage);
    imagesCount ++;

    let selectedHardImage = new Image();
    selectedHardImage.src = 'assets/imgs/states/Selected_hard.png';
    images.push(selectedHardImage);
    imagesCount ++;


    // Incrémentation du nb d'images chargées et affichage quand complet
    for(var i=0; i<images.length; i++){
      images[i].onload = function(){
        imagesLoaded++;

        // Rectangle correspondant a la progression de la barre selon vie du boss
        context.fillStyle = "white";
        context.fillRect(330, 350, imagesLoaded*10, 50);

        if(imagesLoaded == imagesCount){
          // Si déjà une progression locale
          if(localStorage.getItem('username') != undefined) {
            setTimeout(controller.displayExplorationMenu.bind(controller), 1000);
            //controller.music.play();
          } else {
            setTimeout(controller.displayModeMenu.bind(controller), 1000)
            //controller.displayExplorationMenu();
          }
        }
      }
    }
  }

}
