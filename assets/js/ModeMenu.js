/*
  Classe controlant le menu de choix du mode de sauvegarde initial.

  Elle gère :
  - L'affichage des boutons interactifs
  - La création d'un nouveau profil en cas de nouvelle partie
  - L'import d'un profil sous forme de fichier JSON
*/

function ModeMenu() {

  // Coordonnées bouton nouvelle partie
  const newButtonCoords = {
    x: (canvas.width/2) - 400,
    y: 276,
    width: 800,
    height: 83
  }


  // Coordonnées bouton importer profil
  const importButtonCoords = {
    x: (canvas.width/2) - 400,
    y: 464,
    width: 800,
    height: 83
  }


  // Fonction listener de click sur le canvas (pas de lambda expression pour pouvoir la remove apres)
  var modeClickHandler =  function(e) {

    var it = controller.modeMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };

    // Vérifie interactions avec bouton new
    if (isSquareIntersect(mouse, newButtonCoords)) {
      it.removeClickListener();
      it.hideFileSelector();
      controller.displayAvatarSelectionMenu();
    }
  }


  // Fonction d'affichage de la scène pour le menu choix du mode
  ModeMenu.prototype.launchModeMenuScene = function() {
    this.displayMenuBackground();
    this.displayFileSelector();
    //setTimeout(this.displayMenuButtons.bind(this), 100);

    // Ajout du listener via la fonction
    canvas.addEventListener('click', modeClickHandler);
  }


  // Suppression du listener
  ModeMenu.prototype.removeClickListener = function() {
    canvas.removeEventListener('click', modeClickHandler);
  }


  // Affichage du background
  ModeMenu.prototype.displayMenuBackground = function() {
    var background = new Image();
    background.onload = function() {
        context.drawImage(background, 0, 0, 1400, 700);
     }
     background.src = 'assets/imgs/menus/ModeMenu.png';
  }


  // Affichage des boutons
  ModeMenu.prototype.displayMenuButtons = function() {
    // Affichage bouton new
    context.beginPath();
    context.rect(newButtonCoords.x, newButtonCoords.y, newButtonCoords.width, newButtonCoords.height);
    context.stroke();


    // Affichage bouton importer
    context.beginPath();
    context.rect(importButtonCoords.x, importButtonCoords.y, importButtonCoords.width, importButtonCoords.height);
    context.stroke();
  }


  // Affichage de l'explorateur de fichiers pour import
  ModeMenu.prototype.displayFileSelector = function() {
    // Affichage de l'element html input par dessus le canvas
    let input = document.getElementById("inputFileLabel2");
    input.style.visibility = 'visible';
  }


  // Masque de l'explorateur de fichiers pour import
  ModeMenu.prototype.hideFileSelector = function() {
    // Affichage de l'element html input par dessus le canvas
    let input = document.getElementById("inputFileLabel2");
    input.style.visibility = 'hidden';
  }
}
