/*
  Classe correspondant à une notification de jeu.

  Elle contient :
  - Le message à afficher
  - Sa position en x (pour centrer le texte)

  Elle gère :
  - L'affichage et le masque du canvas temporaire à superposer
  - L'affichage de la notification et du message
*/

function Notification(message, x) {

  this.message = message; // Message à afficher
  this.x = x; // Position du texte en x

  // Fonction d'affichage de la notification
  Notification.prototype.displayNotification = function() {
    this.displayCanvas();
    this.displayBackground();
    setTimeout(this.displayText.bind(this), 100);
    setTimeout(this.hideCanvas.bind(this), 2000);
  }

  // Affichage du canvas temporaire
  Notification.prototype.displayCanvas = function() {
    // Récupération et affichage du canvas
    var canvasTmp = document.getElementById('canvasTmp');
    canvasTmp.style.visibility = 'visible';
  }

  // Masque du canvas temporaire
  Notification.prototype.hideCanvas = function() {
    // Récupération et masque du canvas
    var canvasTmp = document.getElementById('canvasTmp');
    contextTmp.clearRect(0, 0, canvasTmp.width, canvasTmp.height);
    canvasTmp.style.visibility = 'hidden';
  }


  // Affichage du background
  Notification.prototype.displayBackground = function() {
    var background = new Image();
    background.onload = function() {
        contextTmp.drawImage(background, 0, 0, 1400, 700);
     }
     background.src = 'assets/imgs/objects/Notification.png';
  }


  // Affichage du texte
  Notification.prototype.displayText = function() {
    contextTmp.fillStyle = "white";
    contextTmp.font = "800 40px Apple SD Gothic Neo";
    contextTmp.fillText(""+this.message, this.x, 92);
  }

}
