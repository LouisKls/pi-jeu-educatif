/*
  Classe correspondant à un objet cosmétique pour l'avatar.

  Elle contient :
  - Le nom de l'objet
  - Le type de l'objet (head, body, hands, legs ou feet)
  - Le prix de l'objet en boutique
  - Les coordonnées de l'affichage de l'objet dans le menu avatar et boutique
*/

function Object(name, type, price) {

  this.name = name; // Nom de l'objet
  this.type = type; // Type (corps, tête ...)
  this.price = price; // Prix de l'objet pour la boutique


  // Coordonnées pour l'affichage dans le menu avatar (trois par ligne)
  let i = controller.player.ownedObjects.length; // Indice de l'objet
  let coeff1 = Math.trunc(i/3); // Indice colonne

  this.x1 = (100 * (i+1)) - (coeff1*300);
  this.y1 = 150 * (coeff1+1);


  // Coordonnées pour l'affichage dans le menu boutique (trois par ligne)
  let j = controller.shopMenu.objectsList.length; // Indice de l'objet
  let coeff2 = Math.trunc(j/3); // Indice colonne

  this.x2 = (100 * (j+1)) - (coeff2*300);
  this.y2 = 150 * (coeff2+1);


  // Dimensions
  this.width = 75;
  this.height = 75;
}
