/*
  Classe controlant le menu d'options du jeu.

  Elle contient :
  - Le joueur et ses informations en paramètre

  Elle gère :
  - L'affichage des boutons interactifs
  - Le changement de difficulté associée au joueur
  - La réinitialisation de la sauvegarde interne
  - L'export du profil sous fichier JSON
  - L'import du profil sous fichier JSON
*/

function OptionsMenu(player) {

  this.player = player; // Objet joueur


  // Coordonnées iconne de fermeture
  const crossIconCoords = {
    x: canvas.width - 106,
    y: 81,
    radius: 50
  }


  // Coordonnées bouton facile
  const easyButtonCoords = {
    x: (canvas.width/2) - 140,
    y: 245,
    width: 240,
    height: 83
  }


  // Coordonnées bouton moyen
  const mediumButtonCoords = {
    x: (canvas.width/2) + 140,
    y: 245,
    width: 240,
    height: 83
  }


  // Coordonnées bouton difficile
  const hardButtonCoords = {
    x: (canvas.width/2) + 420,
    y: 245,
    width: 240,
    height: 83
  }


  // Coordonnées bouton reinitialiser
  const deleteButtonCoords = {
    x: (canvas.width/2) - 140,
    y: 388,
    width: 800,
    height: 81
  }


  // Coordonnées bouton export
  const exportButtonCoords = {
    x: (canvas.width/2) - 140,
    y: 536,
    width: 375,
    height: 86
  }


  // Coordonnées bouton import
  const importButtonCoords = {
    x: (canvas.width/2) + 277,
    y: 536,
    width: 380,
    height: 86
  }


  // Handler de click pour listener
  var optionsMenuClickHandler = function(e) {

    var it = controller.optionsMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // Récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };

    // Vérifie click sur l'iconne de fermeture
    if(isCircleIntersect(mouse, crossIconCoords)) {
      it.hideFileSelector();
      it.closeOptionsMenuScene();
    }

    // Vérifie click sur difficulté facile
    if(isSquareIntersect(mouse, easyButtonCoords)) {
      if(it.player.difficulty != 'easy') {
        it.player.difficulty = 'easy'; // Defini difficulté si pas déja le cas
      }
      // Refresh du menu
      it.removeClickListener();
      controller.clearScene();
      it.launchOptionsMenuScene();
    }

    // Vérifie click sur difficulté moyen
    if(isSquareIntersect(mouse, mediumButtonCoords)) {
      if(it.player.difficulty != 'medium') {
        it.player.difficulty = 'medium';
      }
      // Refresh du menu
      it.removeClickListener();
      controller.clearScene();
      it.launchOptionsMenuScene();
    }

    // Vérifie click sur difficulté difficile
    if(isSquareIntersect(mouse, hardButtonCoords)) {
      if(it.player.difficulty != 'hard') {
        it.player.difficulty = 'hard';
      }
      // Refresh du menu
      it.removeClickListener();
      controller.clearScene();
      it.launchOptionsMenuScene();
    }


    // Vérifie click sur bouton reinitialiser
    if(isSquareIntersect(mouse, deleteButtonCoords)) {
      controller.removeProfile();
    }

    // Vérifie click sur bouton exporter
    if(isSquareIntersect(mouse, exportButtonCoords)) {
      controller.exportProfile();
    }
  }


  // Lancement de la scène
  OptionsMenu.prototype.launchOptionsMenuScene = function() {
    this.displayBackground();
    this.displayFileSelector();
    //setTimeout(this.displayButtons.bind(this), 100);

    // Ajout du listener via la fonction
    canvas.addEventListener('click', optionsMenuClickHandler);
  }


  // Affichage du background
  OptionsMenu.prototype.displayBackground = function() {
    // Stockage des images à charger
     var background = new Image();
     background.src = 'assets/imgs/menus/OptionsMenu.png';

     var select = new Image();
     let difficulty = this.player.difficulty;
     select.src = 'assets/imgs/states/Selected_'+difficulty+'.png';

     // Listes des images
     var images = [background, select];

     // Nombre d'images à charger
     var imagesCount = images.length;

     // Images chargées
     var imagesLoaded = 0;

     // Incrémentation du nb d'images chargées et affichage quand complet
     for(var i=0; i<images.length; i++){
       images[i].onload = function(){
         imagesLoaded++;
         if(imagesLoaded == imagesCount){
           for(var j=0; j<images.length; j++){
             context.drawImage(images[j], 0, 0, 1400, 700);
           }
         }
       }
     }

  }


  // Affichage des boutons
  OptionsMenu.prototype.displayButtons = function() {

    // Affichage iconne de fermeture
    context.beginPath();
    context.arc(crossIconCoords.x, crossIconCoords.y, crossIconCoords.radius, 0, 2 * Math.PI, false);
    context.fillStyle = 'grey';
    context.fill();


     // Bouton difficulté facile
     context.fillRect(easyButtonCoords.x, easyButtonCoords.y, easyButtonCoords.width, easyButtonCoords.height);



     // Bouton difficulté moyenne
     context.fillRect(mediumButtonCoords.x, mediumButtonCoords.y, mediumButtonCoords.width, mediumButtonCoords.height);



     // Bouton difficulté difficile
     context.fillRect(hardButtonCoords.x, hardButtonCoords.y, hardButtonCoords.width, hardButtonCoords.height);


     // Bouton suppression sauvegarde
     context.fillStyle = 'grey';
     context.beginPath();
     context.rect(deleteButtonCoords.x, deleteButtonCoords.y, deleteButtonCoords.width, deleteButtonCoords.height);
     context.stroke();



     // Bouton export profil
     context.fillStyle = 'grey';
     context.beginPath();
     context.rect(exportButtonCoords.x, exportButtonCoords.y, exportButtonCoords.width, exportButtonCoords.height);
     context.stroke();


     // Bouton import profil
     context.fillStyle = 'grey';
     context.beginPath();
     context.rect(importButtonCoords.x, importButtonCoords.y, importButtonCoords.width, importButtonCoords.height);
     context.stroke();

  }



  // Affichage de l'explorateur de fichiers pour import
  OptionsMenu.prototype.displayFileSelector = function() {
    // Affichage de l'element html input par dessus le canvas
    let input = document.getElementById("inputFileLabel1");
    input.style.visibility = 'visible';
  }


  // Masque de l'explorateur de fichiers pour import
  OptionsMenu.prototype.hideFileSelector = function() {
    // Affichage de l'element html input par dessus le canvas
    let input = document.getElementById("inputFileLabel1");
    input.style.visibility = 'hidden';
  }


  // Fermeture de la fenêtre
  OptionsMenu.prototype.closeOptionsMenuScene = function() {
    this.removeClickListener();
    controller.displayExplorationMenu();
  }



  // Suppression du listener de click sur le menu options
  OptionsMenu.prototype.removeClickListener = function() {
    canvas.removeEventListener('click', optionsMenuClickHandler);
  }
}
