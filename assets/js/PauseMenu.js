/*
  Classe controlant le menu pause pour un niveau.

  Elle contient :
  - Le niveau (objet) auquel elle est associée

  Elle gère :
  - L'affichage des boutons interactifs
  - L'affichage de la fenêtre modale
  - La reprise du niveau courant (suppression fenêtre et affichage du niveau)
  - La relance du niveau
  - Le retour au menu principal
  - Le listener
*/

function PauseMenu(level) {

  this.level = level;

  // Coordonnées bouton reprendre
  const resumeButtonCoords = {
    x: (canvas.width/2) - 166,
    y: canvas.height - 375,
    width: 330,
    height: 68
  }

  // Coordonnées bouton recommencer
  const tryAgainButtonCoords = {
    x: (canvas.width/2) - 166,
    y: canvas.height - 272,
    width: 330,
    height: 68
  }

  // Coordonnées bouton menu principal
  const mainMenuButtonCoords = {
    x: (canvas.width/2) - 166,
    y: canvas.height - 167,
    width: 330,
    height: 68
  }



  // Handler de click pour le listener
  var pauseMenuClickHandler = function(e) {

    var it = controller.pauseMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };

    // Vérifie click sur bouton reprendre
    if(isSquareIntersect(mouse, resumeButtonCoords)) {
      let question = it.level.questionDisplay;
      let answer = it.level.questionAnswer;
      let answerDisplay = it.level.answerDisplay;
      let count = it.level.questionsCount;
      let time = it.level.time;
      let playerLives = it.level.playerLives;
      let bossLives = it.level.bossLives;
      it.removeClickListener();
      if(it.level.explorationLevel.type == 'normal' && it.level.world.id != 8 && it.level.world.id != 9) {
        controller.resumeLevel(it.level.world, it.level.explorationLevel, question, answer, answerDisplay, count);
      } else if(it.level.explorationLevel.type == 'normal' && (it.level.world.id == 8 || it.level.world.id == 9)) {
        controller.resumeProblemLevel(it.level.world, it.level.explorationLevel, it.level.problem, answer, count);
      } else if(it.level.explorationLevel.type == 'special') {
        controller.resumeSpecialLevel(it.level.world, it.level.explorationLevel, question, answer, answerDisplay, count, time);
      } else if(it.level.explorationLevel.type == 'boss') {
        controller.resumeBossLevel(it.level.world, it.level.explorationLevel, question, answer, answerDisplay, playerLives, bossLives);
      }

    }

    // Vérifie click sur bouton recommencer
    if(isSquareIntersect(mouse, tryAgainButtonCoords)) {
      it.removeClickListener();
      controller.launchLevel(it.level.world, it.level.explorationLevel);
    }

    // Vérifie click sur bouton menu principal
    if(isSquareIntersect(mouse, mainMenuButtonCoords)) {
      if(it.level.explorationLevel.type == 'special') {
        clearInterval(it.level.timer);
      }
      it.removeClickListener();
      controller.displayExplorationMenu();
    }
  }



  // Affichage du menu options
  PauseMenu.prototype.launchPauseMenuScene = function() {
    if(this.level.explorationLevel.type == 'special') {
      clearInterval(controller.level.timer);
    }

    this.displayMenuBackground();
    // setTimeout(this.displayButtons.bind(this), 100);

    // Ajout du listener via la fonction
    canvas.addEventListener('click', pauseMenuClickHandler);
  }


  // Affichage du background
  PauseMenu.prototype.displayMenuBackground = function() {
    var background = new Image();
    background.onload = function() {
        context.drawImage(background, 0, 0, 1400, 700);
     }
     background.src = 'assets/imgs/menus/PauseMenu.png';
  }



  // Affichage des boutons
  PauseMenu.prototype.displayButtons = function() {
     // Affichage bouton reprendre
     context.beginPath();
     context.rect(resumeButtonCoords.x, resumeButtonCoords.y, resumeButtonCoords.width, resumeButtonCoords.height);
     context.stroke();


     // Affichage bouton recommencer
     context.beginPath();
     context.rect(tryAgainButtonCoords.x, tryAgainButtonCoords.y, tryAgainButtonCoords.width, tryAgainButtonCoords.height);
     context.stroke();


     // Affichage bouton menu principal
     context.beginPath();
     context.rect(mainMenuButtonCoords.x, mainMenuButtonCoords.y, mainMenuButtonCoords.width, mainMenuButtonCoords.height);
     context.stroke();
  }


  // Fermeture de la fenêtre
  PauseMenu.prototype.closePauseMenuScene = function() {
    this.removeClickListener();
    // ?
  }


  // Suppression du listener de click sur le menu options
  PauseMenu.prototype.removeClickListener = function() {
    canvas.removeEventListener('click', pauseMenuClickHandler);
  }

}
