/*
  Classe correspondant au joueur.

  Elle contient :
  - Le pseudo du joueur
  - L'id de l'avatar choisi par défaut
  - Les points d'experience possédés par le joueur
  - Le niveau du joueur
  - L'argent du joueur
  - Les pièces de vaisseau possédées pour le dernier monde débloqué
  - La difficulté choisie
  - La liste des objets qu'il possède
  - La liste (objet) des objets qu'il a équipé pour son avatar
*/

function Player(username) {

  this.username = username; // Nom du joueur
  this.defaultAvatar = 1; // Id de l'avatar par défaut (1 ou 2)
  this.experience = 0; // Points d'experience pour l'augmentation de niveau
  this.level = 1; // Niveau du joueur
  this.money = 0; // Argent du joueur
  this.shipParts = 0; // Pièces de vaisseau récupérées
  this.difficulty = 'easy'; // Difficulté des questions (facile,moyen,difficile)

  // Liste des objets possédés
  this.ownedObjects = [];

  // Liste des objets équipés par type
  this.equipedObjects = {
    head: null,
    body: null,
    legs: null,
    hands: null,
    feet: null
  }

}
