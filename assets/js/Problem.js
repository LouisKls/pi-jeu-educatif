/*
  Classe correspondant à un problème généré.

  Elle contient :
  - Le type du problème qui peut être égal à 'simple ou 'complex' selon la réponse attendue (opération ou résultat)
  - Le prénom affiché dans l'énoncé
  - L'objet de l'énoncé'
  - L'opérateur de l'énoncé
  - La réponse au problème
  - L'affichage des phrases de l'énoncé
  - Les listes de mots disponibles qui seront choisi aléatoirement pour la génération de l'énoncé

  Elle gère :
  - La génération aléatoire de l'énoncé selon chacun des paramètres
  - Le stockage de la réponse attendue selon le type de problème
  - L'affichage du problème
*/

function Problem(type) {

  this.type = type; // Type du problème (simple ou complex)
  this.name = null; // Prénom de l'énoncé
  this.object = null; // Objet de l'énoncé
  this.operator = null; // Opérateur de l'énoncé
  this.answer = null; // Réponse au problème
  this.display = [null,null,null]; // Phrases pour l'affichage

  // Listes les noms et opérateurs possibles pour la génération de l'énoncé
  this.possibleNames = ['Marion','Pierre','Léa','Louis','Jasmine','Jean','Charlotte','Henry','Marie'];
  this.possibleObjects = ['billes','crayons'];
  this.possibleOperators = ['+','-'];
  this.addWords = ['achète','gagne','trouve'];
  this.subWords = ['vend','perd','donne'];

  // Coordonnées d'affichage de l'énoncé
  this.displayCoords = {
    x: (canvas.width/2) - 300,
    y: 200
  }

  // Coordonnées d'affichage de l'énoncé
  var displayCoords = {
    x: (canvas.width/2) - 300,
    y: 200
  }

  // Fonction générant un problème aléatoire selon le type de celui ci
  Problem.prototype.generateProblem = function() {
    // Prénom aléatoire
    let i = Math.floor(Math.random() * 9);
    this.name = this.possibleNames[i];

    // Genre du pronom selon parité de l'indice
    let gender = null;

    if(i%2 == 0) {
      gender = 'Elle';
    } else {
      gender = 'Il';
    }


    // Objet aléatoire
    let j = Math.round(Math.random());
    this.object = this.possibleObjects[j];

    // Opérateur aléatoire
    let k = Math.round(Math.random());
    this.operator = this.possibleOperators[k];


    // Adaptation complexité des nombres selon difficulté
    if(controller.player.difficulty == 'easy') {
      firstNumber = Math.floor(Math.random() * 9) + 1; // Premier membre entre 1 et 9
      secondNumber = Math.floor(Math.random() * firstNumber) + 1; // Second membre entre 1 et le premier nombre (évite négatif)

    } else if(controller.player.difficulty == 'medium') {
      firstNumber = Math.floor(Math.random() * 90) + 10; // Entre 10 et 99
      secondNumber = Math.floor(Math.random() * firstNumber) + 1;

    } else if(controller.player.difficulty == 'hard') {
      firstNumber = Math.floor(Math.random() * 50) + 50; // Entre 50 et 99
      secondNumber = Math.floor(Math.random() * firstNumber) + 1;
    }

    // Stockage de la réponse
    if(this.type == 'simple') {
      this.answer = this.operator;
    } else if(this.type == 'complex') {
      this.answer = eval(firstNumber+" "+this.operator+" "+secondNumber);
    }

    // Affichage des phrases de la question
    this.display[0] = this.name+" possède "+firstNumber+" "+this.object+".";
    this.display[1] = gender+" en "+this.operatorToWord(this.operator)+" "+secondNumber+".";

    if(this.type == 'simple') {
      this.display[2] = "Comment savoir combien "+gender.toLowerCase()+" en a maintenant ?";
    } else if(this.type == 'complex') {
      this.display[2] = "Combien en a t-"+gender.toLowerCase()+" maintenant ?";
    }
  }


  // Fonction affichant l'énoncé du problème
  Problem.prototype.displayProblem = function() {
    // Phrase 1
    context.fillStyle = "white";
    context.font = "800 40px Apple SD Gothic Neo";
    context.fillText(this.display[0], displayCoords.x + 60, displayCoords.y);

    // Phrase 2
    context.fillStyle = "white";
    context.font = "800 40px Apple SD Gothic Neo";
    context.fillText(this.display[1], displayCoords.x + 150, displayCoords.y + 80);

    // Phrase 3
    context.fillStyle = "white";
    context.font = "800 40px Apple SD Gothic Neo";
    if(this.type == 'complex') {
      context.fillText(this.display[2], displayCoords.x + 10, displayCoords.y + 160);
    } else {
      context.fillText(this.display[2], displayCoords.x - 100, displayCoords.y + 160);
    }
  }


  // Fonction générant un mot aléatoire en fonctionne de l'opérateur
  Problem.prototype.operatorToWord = function(operator) {
    // Mot aléatoire associé à l'opérateur
    let i = Math.floor(Math.random() * 3);

    if(operator == '+') {
      return this.addWords[i];
    } else if(operator == '-') {
      return this.subWords[i];
    }
  }

}
