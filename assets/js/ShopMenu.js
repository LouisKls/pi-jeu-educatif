/*
  Classe controlant le menu de la boutique.

  Elle contient :
  - Le joueur et ses informations en paramètre

  Elle gère :


*/

function ShopMenu(player) {

  this.player = player; // Objet joueur
  this.total = 0; // Prix total des objets selectionnés
  this.objectsList = []; // Liste des objets du Jeu

  // Sauvegarde temporaire des objets équipés
  this.tempObjects = {
    head: null,
    body: null,
    hands: null,
    legs: null,
    feet: null
  }


  // Coordonnées iconne de fermeture
  const crossIconCoords = {
    x: canvas.width - 107,
    y: 82,
    radius: 50
  }


  // Coordonnées image avatar
  const avatarImageCoords = {
    x: (canvas.width/2) - 130,
    y: 150,
    width: 250,
    height: 470
  }


  // Coordonnées iconne boutique
  const buyIconCoords = {
    x: (canvas.width/2) + 181,
    y: canvas.height - 38,
    radius: 30
  }


 // Handler de click pour listener
  var shopMenuClickHandler = function(e) {

    var it = controller.shopMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };


    // Vérifie click sur l'iconne de fermeture
    if(isCircleIntersect(mouse, crossIconCoords)) {
      it.restoreEquipedObjects();
      it.closeShopMenuScene();
    }


    // Vérifie click sur l'iconne d'achat
    if(isCircleIntersect(mouse, buyIconCoords)) {
      if(it.canBuy() && it.total != 0) {
        it.buyObjects();
        it.restoreEquipedObjects();
        controller.saveProfile();
        it.closeShopMenuScene();
      } else if(it.total != 0){
        var notif = new Notification("Tu n'as pas assez d'argent !", 450);
        notif.displayNotification();
      }
    }


    // Verifie interactions avec objets
    it.objectsList.forEach(object => {
      if (isShopObjectIntersect(mouse, object)) {
        // Si l'objet n'est pas déjà équipé -> équiper
        if(!it.isEquiped(object.name)) {
          if(object.type == 'head') {
            it.tempObjects.head = object.name;
            it.total += object.price;
          } else if(object.type == 'body') {
            it.tempObjects.body = object.name;
            it.total += object.price;
          } else if(object.type == 'hands') {
            it.tempObjects.hands = object.name;
            it.total += object.price;
          } else if(object.type == 'legs') {
            it.tempObjects.legs = object.name;
            it.total += object.price;
          } else if(object.type == 'feet') {
            it.tempObjects.feet = object.name;
            it.total += object.price;
          }
        // Sinon l'objet est déjà équipé -> déséquiper
        } else {
          if(object.type == 'head') {
            it.tempObjects.head = null;
            it.total -= object.price;
          } else if(object.type == 'body') {
            it.tempObjects.body = null;
            it.total -= object.price;
          } else if(object.type == 'hands') {
            it.tempObjects.hands = null;
            it.total -= object.price;
          } else if(object.type == 'legs') {
            it.tempObjects.legs = null;
            it.total -= object.price;
          } else if(object.type == 'feet') {
            it.tempObjects.feet = null;
            it.total -= object.price;
          }
        }

        it.removeClickListener();
        controller.displayShopMenu(); // Menu rechargé
      }
    });

  }


 // Affichage du menu boutique
  ShopMenu.prototype.launchShopMenuScene = function() {

    this.displayMenuBackground(); // Affichage du background
    setTimeout(this.displayObjects.bind(this), 100); // Affichage des objets possédés
    setTimeout(this.displayAvatar.bind(this), 100); // Affichage avatar

    setTimeout(this.displayText.bind(this), 100); // Affichage du titre
    //setTimeout(this.displayButtons.bind(this), 100); // Affichage boutons


    // Ajout du listener via la fonction
    canvas.addEventListener('click', shopMenuClickHandler);
  }


  // Affichage du background
  ShopMenu.prototype.displayMenuBackground = function() {
    var background = new Image();
    background.onload = function() {
        context.drawImage(background, 0, 0, 1400, 700);
     }
     background.src = 'assets/imgs/menus/ShopMenu.png';
  }


  // Affichage du texte
  ShopMenu.prototype.displayText = function() {

   // Argent du joueur
   context.fillStyle = "#576bc1";
   context.font = "800 25px Apple SD Gothic Neo";
   context.fillText(this.player.money, 160, 85);

   // Total
   context.fillStyle = "white";
   context.font = "800 40px Apple SD Gothic Neo";
  context.fillText(""+this.total, (canvas.width/2)-30, canvas.height-21);

  }


  // Affichage des objets
  ShopMenu.prototype.displayObjects = function() {
    this.objectsList.forEach(object => {
      // // Cadre de l'objet
      // context.beginPath();
      // context.rect(object.x2, object.y2, object.width, object.height);
      // context.stroke();


      // Iconne si selectionné
      if(this.isEquiped(object.name)) {
        var goodIcon = new Image();
        goodIcon.onload = function() {
          context.drawImage(goodIcon,  object.x2+67, object.y2-20, 30, 30); // Affichage svg iconne check
        }
        goodIcon.src = 'assets/imgs/good.svg';
      }

      // Image de l'objet
      let objectImage = new Image();
      objectImage.onload = function() {
          context.drawImage(objectImage, 25, typeToSy(object.type), 200, 200, object.x2, object.y2, object.width, object.height);
       }
       objectImage.src = 'assets/imgs/avatar/'+object.name+'.png';

       // Prix
       context.fillStyle = "white";
       context.font = "800 15px Apple SD Gothic Neo";
       context.fillText(""+object.price, object.x2+18, object.y2+101);

       // Iconne pieces
       var coinsIcon = new Image();
       coinsIcon.onload = function() {
         context.drawImage(coinsIcon, object.x2+60, object.y2+85, 20, 20); // Affichage svg iconne
       }
       coinsIcon.src = 'assets/imgs/objects/coin.png';
    });
  }


  // Affichage de l'avatar
  ShopMenu.prototype.displayAvatar = function() {
     // Affichage avatar basique
     var avatarImage = new Image();
     avatarImage.onload = function() {
         context.drawImage(avatarImage, avatarImageCoords.x, avatarImageCoords.y, avatarImageCoords.width, avatarImageCoords.height);
      }
      avatarImage.src = 'assets/imgs/avatar/avatar_'+this.player.defaultAvatar+'.png';


      // Superpose l'image de l'objet de la tête si selectionné
      if(this.tempObjects.head != null) {
        var headImage = new Image();
        headImage.onload = function() {
            //setTimeout(context.drawImage.bind(context), 100, headImage, avatarImageCoords.x, avatarImageCoords.y, avatarImageCoords.width, avatarImageCoords.height);
            context.drawImage(headImage, avatarImageCoords.x, avatarImageCoords.y, avatarImageCoords.width, avatarImageCoords.height);
         }
         headImage.src = 'assets/imgs/avatar/'+this.tempObjects.head+'.png';
      }


      // Superpose l'image de l'objet du corps si selectionné
      if(this.tempObjects.body != null) {
        var bodyImage = new Image();
        bodyImage.onload = function() {
            context.drawImage(bodyImage, avatarImageCoords.x, avatarImageCoords.y, avatarImageCoords.width, avatarImageCoords.height);
         }
         bodyImage.src = 'assets/imgs/avatar/'+this.tempObjects.body+'.png';
      }


      // Superpose l'image de l'objet des mains si selectionné
      if(this.tempObjects.hands != null) {
        var handsImage = new Image();
        handsImage.onload = function() {
            context.drawImage(handsImage, avatarImageCoords.x, avatarImageCoords.y, avatarImageCoords.width, avatarImageCoords.height);
         }
         handsImage.src = 'assets/imgs/avatar/'+this.tempObjects.hands+'.png';
      }


      // Superpose l'image de l'objet des jambes si selectionné
      if(this.tempObjects.legs != null) {
        var legsImage = new Image();
        legsImage.onload = function() {
            context.drawImage(legsImage, avatarImageCoords.x, avatarImageCoords.y, avatarImageCoords.width, avatarImageCoords.height);
         }
         legsImage.src = 'assets/imgs/avatar/'+this.tempObjects.legs+'.png';
      }


      // Superpose l'image de l'objet des pieds si selectionné
      if(this.tempObjects.feet != null) {
        var feetImage = new Image();
        feetImage.onload = function() {
            context.drawImage(feetImage, avatarImageCoords.x, avatarImageCoords.y, avatarImageCoords.width, avatarImageCoords.height);
         }
         feetImage.src = 'assets/imgs/avatar/'+this.tempObjects.feet+'.png';
      }

     // context.beginPath();
     // context.rect(avatarImageCoords.x, avatarImageCoords.y, avatarImageCoords.width, avatarImageCoords.height);
     // context.stroke();
  }



  // Vérifie si l'objet est équipé
  ShopMenu.prototype.isEquiped = function(name) {
    let equiped = this.tempObjects;
      if(equiped.head == name || equiped.body == name ||
        equiped.hands == name || equiped.legs == name ||
        equiped.feet == name) {
          return true;
      } else {
        return false;
      }
  }



  // Affichage des boutons
  ShopMenu.prototype.displayButtons = function() {
    // Affichage iconne de fermeture
     context.beginPath();
     context.arc(crossIconCoords.x, crossIconCoords.y, crossIconCoords.radius, 0, 2 * Math.PI, false);
     context.fillStyle = 'grey';
     context.fill();


     // Affichage iconne d'achat
     context.beginPath();
     context.arc(buyIconCoords.x, buyIconCoords.y, buyIconCoords.radius, 0, 2 * Math.PI, false);
     context.fillStyle = 'grey';
     context.fill();
  }


  // Restauration des objets équipés
  ShopMenu.prototype.restoreEquipedObjects = function() {
    this.tempObjects.head = null;
    this.tempObjects.body = null;
    this.tempObjects.hands = null;
    this.tempObjects.legs = null;
    this.tempObjects.feet = null;

    this.total = 0;
  }


  // Vérifie la possibilité de l'achat ou non
  ShopMenu.prototype.canBuy = function() {
    if(this.player.money >= this.total) {
      return true;
    } else {
      return false;
    }
  }


  // Achat des objets selectionnés
  ShopMenu.prototype.buyObjects = function() {

    if(this.tempObjects.head != null && this.tempObjects.head != 'null') {
      let price = this.getObjectPrice(this.tempObjects.head);
      this.player.money -= price;
      addObjectToPlayer(this.tempObjects.head);
      removeObjectFromShop(this.tempObjects.head);
    }

    if(this.tempObjects.body != null && this.tempObjects.body != 'null') {
      let price = this.getObjectPrice(this.tempObjects.body);
      this.player.money -= price;
      addObjectToPlayer(this.tempObjects.body);
      removeObjectFromShop(this.tempObjects.body);
    }

    if(this.tempObjects.hands != null && this.tempObjects.hands != 'null') {
      let price = this.getObjectPrice(this.tempObjects.hands);
      this.player.money -= price;
      addObjectToPlayer(this.tempObjects.hands);
      removeObjectFromShop(this.tempObjects.hands);
    }

    if(this.tempObjects.legs != null && this.tempObjects.legs != 'null') {
      let price = this.getObjectPrice(this.tempObjects.legs);
      this.player.money -= price;
      addObjectToPlayer(this.tempObjects.legs);
      removeObjectFromShop(this.tempObjects.legs);
    }

    if(this.tempObjects.feet != null && this.tempObjects.feet != 'null') {
      let price = this.getObjectPrice(this.tempObjects.feet);
      this.player.money -= price;
      addObjectToPlayer(this.tempObjects.feet);
      removeObjectFromShop(this.tempObjects.feet);
    }
  }


  // Récupere le prix d'un objet via son nom
  ShopMenu.prototype.getObjectPrice = function(name) {
    var price = null;
    this.objectsList.forEach(object => {
      if(object.name == name) {
        price = object.price;
      }
    });
    return price;
  }


  // Fermeture de la fenêtre
  ShopMenu.prototype.closeShopMenuScene = function() {
    this.removeClickListener();
    controller.displayExplorationMenu();
  }


  // Suppression du listener de click sur le menu boutique
  ShopMenu.prototype.removeClickListener = function() {
    canvas.removeEventListener('click', shopMenuClickHandler);
  }
}
