/*
  Classe coordonnant controlant l'utilisation d'un son

  Elle contient :
  - La source du son
  - Ses attributs html

  Elle gère :
  - La création du son via html
  - Le lancement du son
  - L'arrêt du son
*/

function Sound(src) {

  this.sound = document.createElement("audio");
  this.sound.src = src;
  this.sound.setAttribute("preload", "auto");
  this.sound.setAttribute("controls", "none");
  this.sound.style.display = "none";
  document.body.appendChild(this.sound);

  this.play = function(){
    this.sound.play();
  }

  this.stop = function(){
    this.sound.pause();
  }
}
