/*
  Classe controlant le menu de choix du nom d'utilisateur.

  Elle gère :
  - L'affichage de la fenêtre modale
  - L'affichage des boutons interactifs
  - L'affichage et la création de la zone d'input de texte
  - La suppression de la zone d'input
  - Le stockage du nom d'utilisateur
  - Le listener
*/

function UsernameMenu() {

  // Coordonnées pour l'affichage de la fenêtre modale
  const menuCoords = {
    x: (canvas.width/2) - 350,
    y: 200,
    width: 700,
    height: 280
  }

  // Coordonnées bouton valider
  const validateButtonCoords = {
    x: (canvas.width/2) + 208,
    y: 472,
    width: 191,
    height: 66
  }


  // Handler de click pour le listener
  var usernameClickHandler = function(e) {

    var it = controller.usernameMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)
    var canvasRect = canvas.getBoundingClientRect(); // récuperation des coordonnées du canvas nécessaire pour determiner position de la souris

    // Coordonnées du pointeur souris lors de l'évenement click
    const mouse = {
      x: e.clientX - canvasRect.left,
      y: e.clientY - canvasRect.top
    };

    // Vérifie click sur bouton valider
    if(isSquareIntersect(mouse, validateButtonCoords)) {
      if(it.isUsernameValid()) {
        it.removeClickListener();
        it.saveUsername();
        it.removeInputBox();
        controller.saveProfile();
        controller.displayIntroMenu();

      } else {
        var notif = new Notification('Tu dois entrer un pseudo avant de valider', 330);
        notif.displayNotification();
      }
    }
  }



  // Handler de la touche Entree
  var usernameEnterHandler = function(e) {

    var it = controller.usernameMenu; // Remplace le this non reconnu, necessaire pour pouvoir remove le handler plus tard (sans bind car impossible avec)

    // Si la touche enfoncée est Entrée, meme reaction que bouton valider
    if(e.keyCode == 13) {
      if(it.isUsernameValid()) {
        it.removeClickListener();
        it.saveUsername();
        it.removeInputBox();
        controller.saveProfile();
        controller.displayIntroMenu();

      } else {
        var notif = new Notification('Tu dois entrer un pseudo avant de valider', 330);
        notif.displayNotification();
      }
    }
  }


  // Affichage du menu choix du pseudo
  UsernameMenu.prototype.launchUsernameMenuScene = function() {
    this.displayMenuBackground();
    this.displayInputArea();
    //setTimeout(this.displayButtons.bind(this), 100);

    // Ajout du listener via la fonction
    canvas.addEventListener('click', usernameClickHandler);
    document.addEventListener('keyup', usernameEnterHandler, false);
  }


  // Affichage du background
  UsernameMenu.prototype.displayMenuBackground = function() {
    var background = new Image();
    background.onload = function() {
        context.drawImage(background, 0, 0, 1400, 700);
     }
     background.src = 'assets/imgs/menus/UsernameMenu.png';
  }


  // Affichage des boutons
  UsernameMenu.prototype.displayButtons = function() {
     // Affichage bouton valider
     context.beginPath();
     context.rect(validateButtonCoords.x, validateButtonCoords.y, validateButtonCoords.width, validateButtonCoords.height);
     context.stroke();
  }


  // Affichage de la zone d'input de texte
  UsernameMenu.prototype.displayInputArea = function() {
    // Création de l'element html input par dessus le canvas
    let input = document.createElement("INPUT");
    input.setAttribute("type", "text");
    input.id = 'inputUsername';
    input.style.fontSize = (35/scaleYCoeff)+'px';
    input.style.border = '1px solid white';
    input.style.outline = 'none';
    input.style.textAlign = 'center';
    input.style.position = 'absolute';
    input.style.width = '18vw';

    if(scaleXCoeff == 1400/640) {
      input.style.height = '50px';
      input.style.marginTop = '194px';
    } else if(scaleXCoeff == 1400/320) {
      input.style.height = '25px';
      input.style.marginTop = '100px';
    } else {
      input.style.height = '90px';
      input.style.marginTop = '324px';
    }


    // On l'associe à la div html englobant le canvas
    let div = document.getElementById('gameDiv');
    div.appendChild(input);

    // On donne le focus à l'input
    input.focus();
  }


  // Stockage du nom d'utilisateur
  UsernameMenu.prototype.saveUsername = function() {
    // On récupere l'entrée clavier dans la balise input
    let input = document.getElementById("inputUsername");
    controller.player.username = input.value;
  }


  // Verification du nom d'utilisateur
  UsernameMenu.prototype.isUsernameValid = function() {
    // On récupere l'entrée clavier dans la balise input
    let input = document.getElementById("inputUsername");
    test = input.value;

    if(test == "" || test == null) {
      return false;
    } else {
      return true;
    }
  }


  // Suppression du listener de click sur le menu options
  UsernameMenu.prototype.removeClickListener = function() {
    canvas.removeEventListener('click', usernameClickHandler);
    document.removeEventListener('keyup', usernameEnterHandler);
  }


  // Suppression de la zone d'input clavier
  UsernameMenu.prototype.removeInputBox = function() {
    // Suppresion de la balise HTML d'input via le parent
    let input = document.getElementById("inputUsername");
    let div = document.getElementById('gameDiv');
    div.removeChild(input);
  }

}
