/*
  Fichier principal nécéssaire à la création du controleur et au lancement du jeu

  - Création du controleur de jeu
  - Chargement du profil interne
  - Lancement du menu de jeu initial
*/

console.log(localStorage);

controller = new Controller();

// Affectation initiale temporaire (TODO: à modifier)
controller.player.ownedObjects.push(new Object('jean','legs', 0));
controller.player.ownedObjects.push(new Object('pull','body', 0));
controller.player.ownedObjects.push(new Object('lunettes','head', 0));
controller.player.ownedObjects.push(new Object('gants','hands', 0));
controller.player.ownedObjects.push(new Object('baskets','feet', 0));

controller.shopMenu.objectsList.push(new Object('pull2','body', 200));


controller.loadProfile(); // Chargement du profil interne
controller.displayLaunchMenu(); // Lancement du menu initial
