/*
  Fichier regroupant quelques fonctions indépendantes utiles au système :

  - Récupération du canvas
  - Détection d'intersection pour listeners de click
  - Retour de coordonnées
  - Lecture de fichier pour import du profil
*/

// Informations relatives au canvas
var canvas = document.getElementById('canvas');
var device = null;
var width = 1400;
var height = 700;
canvas.width = width;
canvas.height = height;
var context = canvas.getContext('2d');

// Canvas temporaire superposé
var canvasTmp = document.getElementById('canvasTmp');
var contextTmp = canvasTmp.getContext('2d');

// Coefficients de scale
var scaleXCoeff = 1;
var scaleYCoeff = 1;

// Responsive intermediaire
if((screen.width <= 1400)) {
  scaleXCoeff = 1400/640;
  scaleYCoeff = 700/400;
}

// Responsive mobile
if((screen.width <= 600) && (screen.height <= 850)) {
  scaleXCoeff = 1400/320;
  scaleYCoeff = 700/200;
}



// // Avertissement sur le rafraichissement de la page
// window.onbeforeunload = function (e) {
//     e = e || window.event;
//     // IE et Firefox
//     if (e) {
//       e.returnValue = 'ATTENTION : En actualisant la page vous perdrez toutes données de jeu non-sauvegardées.';
//     }
//     // Safari
//     return 'ATTENTION : En actualisant la page vous perdrez toutes données de jeu non-sauvegardées.';
// };



// Fonction de vérification de l'intersection entre un point et un cercle
function isCircleIntersect(mouse, circle) {
  return Math.sqrt((mouse.x-(circle.x/scaleXCoeff)) ** 2 + (mouse.y - (circle.y/scaleYCoeff)) ** 2) < (circle.radius/scaleXCoeff);
}



// Fonction de vérification de l'intersection entre un point et un quadrilatère (carré par défaut)
function isSquareIntersect(mouse, square) {
  return (mouse.x >= (square.x/scaleXCoeff) && mouse.x <= (square.x/scaleXCoeff) + (square.width/scaleXCoeff) && mouse.y >= (square.y/scaleYCoeff) && mouse.y <= (square.y/scaleYCoeff) + (square.height/scaleYCoeff));
}


// Fonction de vérification de l'intersection entre un point et un objet du menu avatar
function isAvatarObjectIntersect(mouse, square) {
  return (mouse.x >= (square.x1/scaleXCoeff) && mouse.x <= (square.x1/scaleXCoeff) + (square.width/scaleXCoeff) && mouse.y >= (square.y1/scaleYCoeff) && mouse.y <= (square.y1/scaleYCoeff) + (square.height/scaleYCoeff));
}


// Fonction de vérification de l'intersection entre un point et un objet du menu boutique
function isShopObjectIntersect(mouse, square) {
  return (mouse.x >= (square.x2/scaleXCoeff) && mouse.x <= (square.x2/scaleXCoeff) + (square.width/scaleXCoeff) && mouse.y >= (square.y2/scaleYCoeff) && mouse.y <= (square.y2/scaleYCoeff) + (square.height/scaleYCoeff));
}



// Fonction associant un thème mathématiques à son opérateur
function themeToOperator(theme) {
  if(theme == 'Additions' || theme == 'Additions à trou') {
    return '+';
  } else if(theme == 'Soustractions' || theme == 'Soustractions à trou') {
    return '-';
  } else if(theme == 'Doubles' || theme == 'Multiplications (tables 3 à 5)' || theme == 'Multiplications (tables 6 à 9)') {
    return '*';
  }
}



// Fonction associant la secton en y de l'image au type d'objet pour l'affichage
function typeToSy(type) {
  if(type == 'head') {
    return 10;
  } else if(type == 'body') {
    return 150;
  } else if(type == 'hands') {
    return 210;
  } else if(type == 'legs') {
    return 270;
  } else if(type == 'feet') {
    return 350;
  }
}



// Fonction de lecture de fichier associée à la balise html
var openFile = function(event) {
  var input = event.target;

  var reader = new FileReader();
  reader.onload = function(){
    var dataURL = reader.result;
    var request = new XMLHttpRequest();
    request.open('GET', dataURL);
    request.responseType = 'json';
    request.send();

    request.onload = function() {
      // Si c'est bien un profil
      if(this.response.username != null && this.response.username != 'null' && this.response.username != undefined) {
        controller.hideFileSelectors(); // On masque le selecteur
        controller.importProfile(request.response); // Import du profil via fichier json
        controller.optionsMenu.removeClickListener();
        controller.modeMenu.removeClickListener();

        var notif = new Notification('Profil importe avec succes', 460);
        notif.displayNotification();

        setTimeout(controller.saveProfile.bind(controller),1000); // Sauvegarde auto interne
        setTimeout(controller.displayExplorationMenu.bind(controller),1000); // Affichage du menu exploration
      } else {
        var notif = new Notification('Fichier invalide ou corrompu', 460);
        notif.displayNotification();
      }

    }
  };
  reader.readAsDataURL(input.files[0]);
};



// Ajoute un objet au joueur
function addObjectToPlayer(name) {
  controller.shopMenu.objectsList.forEach(object => {
    if(object.name == name) {
      controller.player.ownedObjects.push(object);
    }
  });
}


// Supprime l'objet de la boutique
function removeObjectFromShop(name) {
  controller.shopMenu.objectsList.forEach(object => {
    if(object.name == name) {
      controller.shopMenu.objectsList.pop(object);
    }
  });
}
